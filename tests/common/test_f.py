import pytest
from pyexpect import expect
from hypothesis import given, settings
import hypothesis.strategies as st

import operator as op
import inspect
import math

from itertools import count
from more_itertools import take

from ad_hoc_utils.common.f import (
    _l, lcurry, _r, rcurry, uncurry, _sort, spread, fanout,
    _c, _map, L, is_monotonic,
    select_keys, round_robin_by, fst, snd,
    bind_parameters, restrict_bindings,
    multimethod, wrapped_multimethod,
    head, last, tail,
    first, second)


@pytest.mark.smoke
def test_smoke_stub():
    pass


@pytest.mark.parametrize('result, bound_args, bound_kwargs, args, kwargs', [
    ((1, 2, 4),
     (1,), {},
     (2, 4), {}),

    # currying does not affect default values...
    ((1, 2, 3),
     (1,), {},
     (2,), {}),

    # ...unless the values are specified explicitly
    ((1, 2, 4),
     (1,), dict(z = 4),
     (2,), {}),

    # keyword arguments can be overriden, user-provided arguments take precedence
    ((1, 2, 5),
     (1,), dict(z = 4),
     (2,), dict(z = 5))])
def test_lcurry_simple(result, bound_args, bound_kwargs, args, kwargs):
    f = lambda x, y, z = 3: (x, y, z)
    expect(_l(f, *bound_args, **bound_kwargs)(*args, **kwargs)).to_equal(result)


@pytest.mark.parametrize('result, bound_args, bound_kwargs, args, kwargs', [
    # currying does not affect default values...
    ((1, 2, 3),
     (2,), {},
     (1,), {}),

    # ...unless the values are specified explicitly
    ((1, 2, 4),
     (4,), {},
     (1, 2), {}),

    ((1, 2, 4),
     (2,), dict(z = 4),
     (1,), {}),

    # keyword arguments can be overriden, bound arguments take precedence
    ((1, 2, 4),
     (2,), dict(z = 4),
     (1,), dict(z = 5))])
def test_rcurry_simple(result, bound_args, bound_kwargs, args, kwargs):
    f = lambda x, y, z = 3: (x, y, z)
    expect(_r(f, *bound_args, **bound_kwargs)(*args, **kwargs)).to_equal(result)


@given(
    st.functions(
        pure = True, like = lambda x, *args: (),
        returns = st.functions(pure = True, like = lambda *args: (), returns = st.integers())),
    st.integers(),
    st.lists(st.integers()))
def test_uncurry_isomorphisms(f, x, args):
    expect(uncurry(f)(x, *args)).to_equal(f(x)(*args))


@given(
    st.functions(pure = True, like = lambda *args: (), returns = st.integers()),
    st.lists(st.integers()),
    st.lists(st.integers()))
def test_lcurry_isomorphisms(f, bound_args, args):
    expect(f(*bound_args, *args)).to_equal(_l(f, *bound_args)(*args))
    expect(lcurry(f)(*bound_args)(*args)).to_equal(_l(f, *bound_args)(*args))


@given(
    st.functions(pure = True, like = lambda *args: (), returns = st.integers()),
    st.lists(st.integers()),
    st.lists(st.integers()))
def test_rcurry_isomorphisms(f, bound_args, args):
    expect(f(*args, *bound_args)).to_equal(_r(f, *bound_args)(*args))
    expect(rcurry(f)(*bound_args)(*args)).to_equal(_r(f, *bound_args)(*args))


def test_select_keys_simple():
    expect(select_keys(
        {'foo': 13, 'bar': 42, 'baz': 108}, ['bar', 'baz'])).to_equal(
            {'bar': 42, 'baz': 108})


@pytest.mark.parametrize('result, xs', [
    (True, [1, 2, 3, 4]),
    (True, [4, 3, 2, 1]),
    (True, [1, 2]),
    (True, [1]),
    (True, []),
    (False, [1, 4, 3, 2]),
    (False, [2, 3, 4, 1])])
def test_is_monotonic_simple(result, xs):
    expect(is_monotonic(xs)).to_equal(result)


@given(st.lists(st.integers()))
def test_is_monotonic_gen(xs):
    r = is_monotonic(xs)
    expect(is_monotonic(xs[::-1])).to_equal(r)
    expect(is_monotonic(_c(list, _map(op.neg))(xs))).to_equal(r)


@pytest.mark.parametrize('xs', [
    [(i // n, i % n) for i in range(25)]
    for n in {1, 2, 3, 4, 5, 6}])
def test_round_robin_by_simple(xs):
    expect(_c(is_monotonic, list, _map(snd), round_robin_by(fst))(xs)).to_be(True)
    expect(_c(is_monotonic, list, _map(fst), round_robin_by(snd))(xs)).to_be(True)


@given(st.lists(st.integers(min_value = 0, max_value = 5)))
def test_round_robin_by_invariant(keys):
    expect(_c(set, _map(fst), round_robin_by(snd), enumerate)(keys)).to_equal(
        _c(set, range, len)(keys))


def sample_f_base(foo, bar, *args, baz, qux, **kwargs): pass
sample_f_base_bindings = bind_parameters(sample_f_base, [1, 2, 3, 4], dict(baz = 5, qux = 6, quux = 7))

def sample_f1(foo): pass
def sample_f2(foo, bar, quux): pass
def sample_f3(foo, bar, *misc): pass
def sample_f4(foo, bar, quux, *misc): pass
def sample_f5(*, baz): pass
def sample_f6(*, baz, **misc): pass
def sample_f7(*, baz, qux, **misc): pass

@pytest.mark.parametrize('f,result', [
        (sample_f1, dict(foo = 1)),
        (sample_f2, dict(foo = 1, bar = 2, quux = 3)),
        (sample_f3, dict(foo = 1, bar = 2, misc = (3, 4))),
        (sample_f4, dict(foo = 1, bar = 2, quux = 3, misc = (4,))),
        (sample_f5, dict(baz = 5)),
        (sample_f6, dict(baz = 5, misc = dict(quux = 7))),
        (sample_f7, dict(baz = 5, qux = 6, misc = dict(quux = 7)))])
def test_restrict_bindings(f, result):
    s = inspect.signature(f)
    args, kwargs = restrict_bindings(s, sample_f_base_bindings)
    expect(bind_parameters(f, args, kwargs).arguments).to_equal(result)



def test_multimethod_case1():
    @multimethod(key = 'mode', force = True)
    def agg(xs, *, mode, **kwargs):
        '''Aggregator multimethod'''

    @agg.register
    def agg_max(xs):
        '''Returns maximum'''

        return max(xs)

    @agg.register
    def agg_min(xs):
        '''Returns minimum'''

        return min(xs)

    @agg.register
    def agg_quantile(xs, *, q):
        '''Returns quantile'''

        return _c(L[-1], L[:int(len(xs)*q)], list, _sort())(xs)

    expect(agg([1, 2, 3, 4, 5], mode = 'min')).to_equal(1)
    expect(agg([1, 2, 3, 4, 5], mode = 'max')).to_equal(5)
    expect(agg([1, 2, 3, 4, 5], mode = 'quantile', q = 0.8)).to_equal(4)


def test_multimethod_case2():
    @wrapped_multimethod(key = 'mode')
    def mean(callback, xs, *, mode, x = 4):
        if len(xs) == 0:
            return None
        return callback(xs)

    @mean.register
    def mean_arithmetic(xs):
        return sum(xs)/len(xs)

    @mean.register
    def mean_geometric(xs):
        return math.exp(_c(sum, _map(math.log))(xs)/len(xs))

    @mean.register
    def mean_harmonic(xs):
        return _c(
            spread(op.truediv),
            fanout(len, _c(sum, _map(_l(op.truediv, 1)))))(xs)


    expect(int(mean([1, 2, 3, 4, 5], mode = 'arithmetic'))).to_equal(3)
    expect(int(mean([2, 3, 8, 9, 18], mode = 'geometric'))).to_equal(6)
    expect(int(mean([1, 3, 6, 10, 15], mode = 'harmonic'))).to_equal(3)
    expect(mean([], mode = 'arithmetic')).to_be(None)


def test_multimethod_case3():
    @multimethod(key = 'type_')
    def coerce_type(type_, x):
        pass

    coerce_type.register('int', lambda x: int(x))
    coerce_type.register('str', lambda x: str(x))

    @coerce_type.register
    def coerce_type_float(x):
        return float(x)

    expect(coerce_type('int', 14.5)).to_equal(14)
    expect(coerce_type('str', 14.5)).to_equal('14.5')
    expect(coerce_type('float', 14.5)).to_equal(14.5)


def test_multimethod_case4():
    @multimethod(
        key = lambda lhs, rhs: _c(tuple, _map(lambda x: type(x).__name__))([lhs, rhs]),
        name_to_key = _c(tuple, L.split('_')))
    def custom_add(lhs, rhs):
        pass

    custom_add.register(('int', 'int'), lambda lhs, rhs: lhs + rhs)
    custom_add.register(('str', 'int'), lambda lhs, rhs: lhs + str(rhs))
    custom_add.register(('int', 'str'), lambda lhs, rhs: str(lhs) + rhs)

    @custom_add.register
    def custom_add_str_str(lhs, rhs):
        return lhs + rhs

    expect(custom_add(4, 2)).to_equal(6)
    expect(custom_add('4', 2)).to_equal('42')
    expect(custom_add(4, '2')).to_equal('42')
    expect(custom_add('4', '2')).to_equal('42')


def test_multimethod_case5():
    @multimethod(key = 'mode')
    def distance(*xs, mode): pass

    @distance.register
    def distance_taxicab(*xs):
        return _c(sum, _map(abs))(xs)

    @distance.register
    def distance_euclidian(*xs):
        return _c(math.sqrt, sum, _map(L**2))(xs)

    @distance.register
    def distance_supremum(*xs):
        return _c(max, _map(abs))(xs)

    expect(distance.taxicab(1, 2, 3)).to_equal(6)
    expect(distance.supremum(3, 4, 2, 1)).to_equal(4)
    expect(int(distance.euclidian(1, 4, 8))).to_equal(9)



@pytest.mark.parametrize('result, xs', [
    (None, []),
    (1, [1, 2, 3]),
    (0, range(10)),
    (9, reversed(range(10))),
    (0, count())])
def test_head_simple(result, xs):
    expect(head(xs)).to_equal(result)


@pytest.mark.parametrize('result, xs', [
    (None, []),
    (3, [1, 2, 3]),
    (9, range(10))])
def test_last_simple(result, xs):
    expect(last(xs)).to_equal(result)


@pytest.mark.parametrize('result, xs', [
    ([], []),
    ([2, 3], [1, 2, 3]),
    (range(1, 10), range(10)),
    (reversed(range(9)), reversed(range(10))),
    (count(1), count(0))])
def test_tail_simple(result, xs):
    preprocess = _c(list, _l(take, 10))
    expect(preprocess(tail(xs))).to_equal(preprocess(result))


@pytest.mark.parametrize('result, xs', [
    ((2, 2), (1, 2)),
    ((5, 5, 6), (4, 5, 6)),

    ([2, 2], [1, 2]),
    ([5, 5, 6], [4, 5, 6]),

    ({0: 2}, {0: 1}),
    ({0: 2, 1: 2}, {0: 1, 1: 2})])
def test_first_simple(result, xs):
    xs0 = (xs if isinstance(xs, tuple) else xs.copy())
    expect(first(L + 1)(xs)).to_equal(result)
    expect(xs).to_equal(xs0)


@pytest.mark.parametrize('result, xs', [
    ((1, 3), (1, 2)),
    ((4, 6, 6), (4, 5, 6)),

    ([1, 3], [1, 2]),
    ([4, 6, 6], [4, 5, 6]),

    ({1: 2}, {1: 1}),
    ({0: 1, 1: 3}, {0: 1, 1: 2})])
def test_second_simple(result, xs):
    xs0 = (xs if isinstance(xs, tuple) else xs.copy())
    expect(second(L + 1)(xs)).to_equal(result)
    expect(xs).to_equal(xs0)
