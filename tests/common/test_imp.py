import pytest
from pyexpect import expect
import logging
import sys
from configparser import ConfigParser

from ad_hoc_utils.common.imp import *


@pytest.mark.smoke
def test_smoke_stub():
    pass


@pytest.mark.parametrize('raw_data,result', [
    pytest.param(
        '''
        [options.extras_require]
        foo =
          bar

        baz =
          qux
          quux
        ''',

        dict(
            foo = ['bar'],
            baz = ['quux', 'qux']),

        id = 'simple'),

    pytest.param(
        '''
        [options.extras_require]
        foo =
          foo_deps

        bar =
          @foo
          bar_deps

        baz =
          @foo
          baz_deps
        ''',

        dict(
            foo = ['foo_deps'],
            bar = ['bar_deps', 'foo_deps'],
            baz = ['baz_deps', 'foo_deps']),

        id = 'with_deps'),

    pytest.param(
        '''
        [options.extras_require]
        foo =
          foo_deps

        bar =
          @foo
          bar_deps

        baz =
          @bar
          baz_deps
        ''',

        dict(
            foo = ['foo_deps'],
            bar = ['bar_deps', 'foo_deps'],
            baz = ['bar_deps', 'baz_deps', 'foo_deps']),

        id = 'with_nested_deps')])

def test_parse_config_deps(raw_data, result):
    cfg = ConfigParser()
    cfg.read_string(raw_data)

    expect(parse_config_deps(cfg['options.extras_require'])).to_equal(result)
