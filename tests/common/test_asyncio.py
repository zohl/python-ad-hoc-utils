import pytest
from pyexpect import expect
from hypothesis import given, settings, Phase
import hypothesis.strategies as st
from unittest.mock import Mock

import asyncio
from ad_hoc_utils.common.f import _c, _l, _r, _map, _filter, _sort, L, spread, P, snd, fst, cumsum
from ad_hoc_utils.common.asyncio import *

import time
from itertools import repeat, chain, groupby


@pytest.mark.smoke
def test_smoke_stub():
    pass


def get_dt_estimation(scenario, limits):
    return (
        1.0 if len(limits) == 0
        else max([int(
            (max(0, (scenario['n_calls'] - limit['n_calls'])) + limit['n_calls'] - 1)
            // limit['n_calls']
            * limit['period']
            * 10**9) for limit in limits]))


_delay_threshold = 0.05


def get_max_load(ts, period):
    return _c(
        max, cumsum, list,
        _map(sum, list, _map(snd), snd),
        _r(groupby, _c(L // int(0.05 * (10**9)), fst)),
        _sort(fst), chain)(
        zip(ts, repeat(1)),
        zip(map(L + int(10**9 * period), ts), repeat(-1)))


st_limit = lambda: st.builds(
    lambda **kwargs: kwargs,
    n_calls = st.integers(min_value = 1, max_value = 10),
    period = st.floats(
        min_value = 0.01, max_value = 2.0,
        allow_nan = False, allow_infinity = False, allow_subnormal = False))


st_scenario = lambda: st.builds(
    lambda **kwargs: kwargs,
    n_calls = st.integers(min_value = 1, max_value = 100),
    delay = st.floats(
        min_value = 0.0, max_value = 2.0,
        allow_nan = False, allow_infinity = False, allow_subnormal = False))


async def _run_map(f, xs):
    f0 = Mock()
    t0 = time.monotonic_ns()

    tasks = []
    for x in xs:
        tasks.append(asyncio.create_task(f(x, callback = f0)))
        await asyncio.sleep(0)
    await asyncio.gather(*tasks)

    dt = time.monotonic_ns() - t0

    (ts, xs1) = (
        ([], []) if len(xs) == 0
        else _c(list, _map(list), spread(zip), _map(P.args), P.call_args_list)(f0))

    return (dt, ts, xs1)




@pytest.mark.asyncio
@settings(
    deadline = None,
    derandomize = True)
@given(st.tuples(
    st_scenario(),
    st.lists(st_limit(), min_size = 0, max_size = 4))
       .filter(_c(L < 5.0, L / 10**9, spread(get_dt_estimation))))
async def test_rate_limit(data):
    (scenario, limits) = data

    xs = list(range(scenario['n_calls']))

    delay = scenario.get('delay', 0)
    async def f(x, callback):
        callback(time.monotonic_ns(), x)
        await asyncio.sleep(delay)

    dt_estim = get_dt_estimation(scenario, limits)
    (dt_base, *_) = await _run_map(f, xs)
    (dt, ts, xs1) = await _run_map(_c(*map(_l(spread(rate_limit), []), limits))(f), xs)

    expect(set(xs1)).to_equal(set(xs))
    expect(abs(dt - (dt_base + dt_estim))/scenario['n_calls']/10**9).to_be.less_than(_delay_threshold)
    for limit in limits:
        expect(get_max_load(ts, limit['period'])).to_be.less_or_equal(limit['n_calls'])

