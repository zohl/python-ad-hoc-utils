import pytest
from pyexpect import expect
from hypothesis import given, settings
import hypothesis.strategies as st

import datetime

import operator as op
from ad_hoc_utils.common.datetime import timestamps_between, last_day, trunc_day


@pytest.mark.smoke
def test_smoke_stub():
    pass


@given(st.datetimes())
def test_last_day(dt):
    expect((last_day(dt) + datetime.timedelta(days = 1)).day).to_equal(1)


@given(st.datetimes())
def test_trunc_day(dt):
    r = trunc_day(dt)
    expect((r.year, r.month, r.day, r.hour, r.minute, r.second)).to_equal(
        (dt.year, dt.month, dt.day, 0, 0, 0))


@pytest.mark.parametrize('result, args', [
    (1, (datetime.time(4, 0), datetime.datetime(2020, 1, 1, 12), datetime.datetime(2020, 1, 2, 12))),
    (1, (datetime.time(4, 0), datetime.datetime(2020, 1, 2, 2),  datetime.datetime(2020, 1, 2, 12))),
    (0, (datetime.time(4, 0), datetime.datetime(2020, 1, 1, 12), datetime.datetime(2020, 1, 2, 2))),
    (3, (datetime.time(4, 0), datetime.datetime(2020, 1, 1, 12), datetime.datetime(2020, 1, 4, 12))),
    (2, (datetime.time(4, 0), datetime.datetime(2020, 1, 1, 12), datetime.datetime(2020, 1, 4, 2)))])
def test_timestampts_between(result, args):
    expect(timestamps_between(*args)).to_equal(result)
