from ad_hoc_utils.common.imp import extras

extras.require_deps('ad_hoc_utils', 'ml')


import pytest
from pyexpect import expect

import numpy as np
import hypothesis.extra.numpy as st_np

import string


from hypothesis import given, settings, HealthCheck
import hypothesis.strategies as st
from unittest.mock import Mock

from ad_hoc_utils.common.test import st_seed
from ad_hoc_utils.common.algo import binary_search, simulated_annealing, mk_dp_solver



# TODO:
# algo_exports = dir(algo)
# expect(algo_exports).to_contain('binary_search')


def test_binary_search_simple():
    eps = 1e-6

    f = lambda x: 0.8 * x - 5
    x = binary_search(lambda x: f(x) > 0, -10, 20, eps)

    expect(abs(f(x))).to_be.less_than(eps)


@given(
    st.floats(min_value = 0.1, max_value = 100),
    st.floats(min_value = -100, max_value = 100),
    st.floats(min_value = 1e-12, max_value = 1e-4),
    st.floats(min_value = 1, max_value = 1000),
    st.floats(min_value = 1, max_value = 1000))
def test_binary_search_generic(k, b, eps, l_margin, r_margin):
    f = lambda x: k * x + b
    x = binary_search(lambda x: f(x) > 0, -b / k - l_margin, -b / k + r_margin, eps)
    expect(abs(x + b / k)).to_be.less_than(eps)


@pytest.mark.uat
def test_binary_stub():
    pass


@given(
    st.integers(min_value = 2, max_value = 20).flatmap(lambda n: st.tuples(
        st.permutations(range(n)),
        st_np.arrays(shape = (n, n), dtype = np.int16))),
    st_seed())
def test_simulated_annealing_generic(data, seed):
    """
    Generates and solves travelling salesman problem.

    We expect that Simulated Annealing can find a better than random solution.
    """

    (s0, adj) = data

    def t_mutate(s, rng, t):
        (i, j) = sorted(rng.choice(range(len(s)), size = 2, replace = False))
        return [*s[:i], s[j], *s[i+1:j], s[i], *s[j+1:]]

    def t_evaluate(s):
        r = 0
        for i, j in zip(s, s[1:]):
            r += adj[i][j]
        return r

    s1 = simulated_annealing(s0, t_evaluate, t_mutate, n_iter = 100, seed = seed)
    expect(t_evaluate(s1)).to_be.less_or_equal(t_evaluate(s0))


@pytest.fixture(scope = 'function')
def fib():
    return mk_dp_solver(
        (0, 1),
        (1, 1),
        lambda d, n: d[n - 1] + d[n - 2])


def test_mk_dp_solver_fib_base(fib):
    expect(fib[0]).to_equal(1)
    expect(fib[1]).to_equal(1)


@settings(suppress_health_check = [HealthCheck.function_scoped_fixture])
@given(st.integers(min_value = 2, max_value = 100))
def test_mk_dp_solver_fib_generic(fib, n):
    expect(fib[n]).to_equal(fib[n - 1] + fib[n - 2])


@given(st.lists(st.integers(min_value = 2, max_value = 100), min_size = 1, max_size = 10))
def test_mk_dp_solver_fib_caching(ns):
    f = Mock(wraps = lambda d, n: d[n - 1] + d[n - 2])
    fib = mk_dp_solver((0, 1), (1, 1), f)
    for n in ns:
        fib[n]
    expect(len(f.call_args_list)).to_equal(max(ns) - 1)


def levenshtein_distance(s1, s2):
    d = mk_dp_solver(
        (lambda i, j: i == 0 or j == 0, 0),
        lambda d, i, j: min(
            d[i - 1, j - 1] + (0 if s1[i - 1] == s2[j - 1] else 1),
            d[i - 1, j] + 1,
            d[i, j - 1] + 1))

    return d[len(s1), len(s2)]


def levenshtein_distance_simple(s1, s2):
    d = [[None for _ in range(1 + len(s2))] for _ in range(1 + len(s1))]

    for j in range(1 + len(s2)):
        d[0][j] = 0

    for i in range(1 + len(s1)):
        d[i][0] = 0

    def calc(i, j):
        if d[i][j] is not None:
            return d[i][j]
        r = min(
            calc(i - 1, j - 1) + (0 if s1[i - 1] == s2[j - 1] else 1),
            calc(i - 1, j) + 1,
            calc(i, j - 1) + 1)
        d[i][j] = r
        return r

    return calc(len(s1), len(s2))


@given(
    st.text(alphabet = string.ascii_lowercase, max_size = 50),
    st.text(alphabet = string.ascii_lowercase, max_size = 50))
def test_mk_test_mk_dp_solver_levenshtein_distance(s1, s2):
    expect(levenshtein_distance(s1, s2)).to_equal(levenshtein_distance_simple(s1, s2))
