import pytest
from pyexpect import expect

import datetime
from ad_hoc_utils.common.codec import from_json, to_json


@pytest.mark.smoke
def test_smoke_stub():
    pass


# def test_simple_cases():
#     for v in [
#             42,
#             'foo bar',
#             ['a', 'b', 'x', 'z'],
#             dict(foo = 42, bar = 'aaa'),
#             [dict(foo = [dict(bar = 1), dict(bar = 2)])]]:
#
#         expect(from_json(v)).to_equal(v)
#         expect(to_json(v)).to_equal(v)
#
#
# def test_A():
#     dt = datetime.datetime.now()
#     ehs = [
#         (datetime.datetime,
#          (lambda f, x: x.isoformat(),
#           lambda f, x: datetime.datetime.fromisoformat(x)))]
#
#     expect(to_json(
#         dt,
#         extra_handlers = ehs)).to_equal(
#             dt.isoformat())
#
#     expect(from_json(
#         dt.isoformat(),
#         extra_handlers = ehs)).to_equal(dt.isoformat())
#
#     expect(from_json(
#         dt.isoformat(),
#         extra_handlers = ehs,
#         handler = datetime.datetime)).to_equal(dt)
#
#
# def test_B():
#     dt = datetime.datetime.now()
#     ehs = [
#         (datetime.datetime,
#          (lambda f, x: x.isoformat(),
#           lambda f, x: datetime.datetime.fromisoformat(x))),
#
#         ('struct', dict(c_dt = datetime.datetime))]
#
#     expect(to_json(
#         dict(foo = 42, c_dt = dt),
#         extra_handlers = ehs,
#         handler = 'struct')).to_equal(
#             dict(foo = 42, c_dt = dt.isoformat()))
#
#     expect(from_json(
#         dict(foo = 42, c_dt = dt.isoformat()),
#         extra_handlers = ehs,
#         handler = 'struct')).to_equal(
#             dict(foo = 42, c_dt = dt))
#
#
# def test_C():
#     ehs = [
#         ('root', dict(
#             val1 = 'struct',
#             val2 = (lambda f, x: '|'.join(map(f, x)),
#                     lambda f, x: x.split('|')))),
#         ('struct', (lambda f, x: ':'.join(map(f, x)),
#                     lambda f, x: x.split(':')))]
#
#     expect(to_json(
#         dict(
#             val1 = ['foo', 'bar', 'baz'],
#             val2 = ['qux', 'corge', 'grault']),
#         extra_handlers = ehs,
#         handler = 'root')).to_equal(
#             dict(
#                 val1 = 'foo:bar:baz',
#                 val2 = 'qux|corge|grault'))
#
#     expect(from_json(
#         dict(
#             val1 = 'foo:bar:baz',
#             val2 = 'qux|corge|grault'),
#         extra_handlers = ehs,
#         handler = 'root')).to_equal(
#             dict(
#                 val1 = ['foo', 'bar', 'baz'],
#                 val2 = ['qux', 'corge', 'grault']))
#
#
# def test_D():
#     ehs = [
#         ('root', ['struct']),
#         ('struct', dict(
#             x = (lambda f, x: str(x),
#                  lambda f, x: int(x))))]
#
#     expect(to_json(
#         [dict(x = 1), dict(x = 2), dict(x = 3)],
#         extra_handlers = ehs,
#         handler = 'root')).to_equal(
#             [dict(x = '1'), dict(x = '2'), dict(x = '3')])
#
#     expect(from_json(
#         [dict(x = '1'), dict(x = '2'), dict(x = '3')],
#         extra_handlers = ehs,
#         handler = 'root')).to_equal(
#             [dict(x = 1), dict(x = 2), dict(x = 3)])
