from ad_hoc_utils.common.imp import extras

extras.require_deps('ad_hoc_utils', 'ml')


from pyexpect import expect
import pytest

import numpy as np
import pandas as pd
import sqlalchemy.types as sqlt
import datetime
from itertools import chain

from ad_hoc_utils.common.f import lift_maybe, _c, _map, spread, L, _filter, none_type

from ad_hoc_utils.common.db import (
    refine_column_type, mk_type_digest, detect_column_type_hint, detect_column_type, sql_coerce_type)


default_cfg = dict(
    float_threshold = 1e-8,
    smallint_threshold = 2**(16 - 1)/10,
    integer_threshold = 2**(32 - 1)/10,
    bigint_threshold = 2**(64 - 1)/10)


@pytest.mark.parametrize('t', [sqlt.Numeric, sqlt.String, sqlt.Date])
def test_refine_column_type_empty(t):
    expect(refine_column_type(default_cfg, t, np.array([]))).to_be.instance_of(t)


@pytest.mark.parametrize('t', [sqlt.Numeric, sqlt.String, sqlt.Date])
def test_refine_column_type_all_none(t):
    expect(refine_column_type(default_cfg, t, np.array([None]))).to_be.instance_of(t)


@pytest.mark.parametrize('t', [
    sqlt.Numeric,
    sqlt.SmallInteger,
    sqlt.Integer,
    sqlt.BigInteger,
    sqlt.Float])
def test_refine_column_type_number_hints(t):
    expect(refine_column_type(default_cfg, t, np.array([0]))).to_be.instance_of(sqlt.SmallInteger)


@pytest.mark.parametrize('t', [
    sqlt.String,
    sqlt.Unicode])
def test_refine_column_type_string_hints(t):
    expect(refine_column_type(default_cfg, t, np.array(['']))).to_be.instance_of(sqlt.String)


@pytest.mark.parametrize('t', [
    sqlt.Date,
    sqlt.DateTime])
def test_refine_column_type_date_hints(t):
    expect(refine_column_type(
        default_cfg, t, np.array([datetime.datetime.now()]))).to_be.instance_of(sqlt.DateTime)


constructors = [
    list,
    np.array,
    lambda xs: pd.Series(xs) if len(xs) > 0 else pd.Series(xs, dtype = object)]


test_cases = [
    # numeric cases
    dict(
        data = [1, 2, -512, 1024],
        refined_type = sqlt.SmallInteger,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [1, 2, -512, 1024, None],
        refined_type = sqlt.SmallInteger,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [10000, -20000, 30000],
        refined_type = sqlt.Integer,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [10000, -20000, 30000, None],
        refined_type = sqlt.Integer,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [2**32 + 1, 2**33 - 1],
        refined_type = sqlt.BigInteger,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [2**32 + 1, 2**33 - 1, None],
        refined_type = sqlt.BigInteger,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [2**64 + 1, 2**64 - 1],
        refined_type = sqlt.Numeric,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [2**64 + 1, 2**64 - 1, None],
        refined_type = sqlt.Numeric,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [1.0, 3.14, 2.71828],
        refined_type = sqlt.Float,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [1.0, 3.14, 2.71828, None],
        refined_type = sqlt.Float,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [1.0, 3.14, 2.71828, np.nan],
        refined_type = sqlt.Float,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [1.0, 3.14, 2.71828, pd.NA],
        refined_type = sqlt.Float,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    # text cases
    dict(
        data = ['foo', 'bar', 'baz'],
        refined_type = sqlt.String,
        generic_type = sqlt.String,
        type_digest = (str,),
        length = 3),

    dict(
        data = ['foo', 'bar', 'baz', None],
        refined_type = sqlt.String,
        generic_type = sqlt.String,
        type_digest = (str,),
        length = 3),

    dict(
        data = ['foo', 'bar', 'corge'],
        refined_type = sqlt.String,
        generic_type = sqlt.String,
        type_digest = (str,),
        length = 5),

    dict(
        data = ['foo', 'bar', 'corge', None],
        refined_type = sqlt.String,
        generic_type = sqlt.String,
        type_digest = (str,),
        length = 5),

    # date cases
    dict(
        data = [
            datetime.datetime(2022, 1, 1),
            datetime.datetime(2022, 2, 1),
            datetime.datetime(2022, 3, 1)],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            datetime.datetime(2022, 1, 1),
            datetime.datetime(2022, 2, 1),
            datetime.datetime(2022, 3, 1, 16, 20)],
        refined_type = sqlt.DateTime,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            datetime.datetime(2022, 1, 1),
            datetime.datetime(2022, 2, 1),
            datetime.datetime(2022, 3, 1),
            None],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            pd.Timestamp(2022, 1, 1),
            pd.Timestamp(2022, 2, 1),
            pd.Timestamp(2022, 3, 1)],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            pd.Timestamp(2022, 1, 1),
            pd.Timestamp(2022, 2, 1),
            pd.Timestamp(2022, 3, 1, 16, 20)],
        refined_type = sqlt.DateTime,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            pd.Timestamp(2022, 1, 1),
            pd.Timestamp(2022, 2, 1),
            pd.Timestamp(2022, 3, 1),
            pd.NaT],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            np.datetime64(1, 'D'),
            np.datetime64(2, 'D'),
            np.datetime64(3, 'D')],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            np.datetime64(1, 'D'),
            np.datetime64(2, 'D'),
            np.datetime64(3, 's')],
        refined_type = sqlt.DateTime,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            np.datetime64(1, 'D'),
            np.datetime64(2, 'D'),
            np.datetime64(3, 'D'),
            np.datetime64('NaT')],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            datetime.datetime(2022, 1, 1),
            pd.Timestamp(2022, 2, 1),
            np.datetime64(3, 'D')],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    dict(
        data = [
            datetime.datetime(2022, 1, 1),
            None,
            pd.Timestamp(2022, 2, 1),
            pd.NaT,
            np.datetime64(3, 'D'),
            np.datetime64('NaT')],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,)),

    # edge cases
    dict(
        data = [],
        refined_type = none_type,
        generic_type = none_type,
        type_digest = ()),

    dict(
        data = [None],
        refined_type = none_type,
        generic_type = none_type,
        type_digest = ()),

    dict(
        data = [np.nan],
        refined_type = sqlt.Numeric,
        generic_type = sqlt.Numeric,
        type_digest = (float,)),

    dict(
        data = [np.datetime64('NaT')],
        refined_type = sqlt.Date,
        generic_type = sqlt.Date,
        type_digest = (datetime.datetime,))]


@pytest.mark.parametrize('c', constructors)
@pytest.mark.parametrize('t', test_cases)
def test_refine_column_type(c, t):
    r = refine_column_type(
        default_cfg, t['generic_type'],
        c(t['data']))

    expect(r).to_be.instance_of(t['refined_type'])
    if t['generic_type'] == sqlt.String:
        expect(r.length).to_equal(t['length'])


@pytest.mark.parametrize('c', constructors)
@pytest.mark.parametrize('t', test_cases)
def test_mk_type_digest(c, t):
    expect(mk_type_digest(c(t['data']))).to_equal(t['type_digest'])


@pytest.mark.parametrize('c', constructors)
@pytest.mark.parametrize('t', test_cases)
def test_detect_column_type_hint(c, t):
    expect(detect_column_type_hint(c(t['data']))).to_equal(t['generic_type'])


@pytest.mark.parametrize('c', constructors)
@pytest.mark.parametrize('t', test_cases)
def test_detect_column_type(c, t):
    expect(detect_column_type(c(t['data']))).to_be.instance_of(t['refined_type'])


@pytest.mark.parametrize('result, t, x', [
    (42, sqlt.BigInteger(), 42),
    (42, sqlt.BigInteger(), 42.5),
    (None, sqlt.BigInteger(), None),
    (None, sqlt.BigInteger(), np.nan),
    (None, sqlt.BigInteger(), pd.NA)])
def test_sql_coerce_type(result, t, x):
    expect(sql_coerce_type(t, x)).to_equal(result)
