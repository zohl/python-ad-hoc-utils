from ad_hoc_utils.common.imp import import_tests_from_submodules

import pytest


import_tests_from_submodules(globals(), __name__, '.ml')


@pytest.mark.smoke
def test_smoke_stub():
    pass
