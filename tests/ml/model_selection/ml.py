from ad_hoc_utils.common.imp import extras

extras.require_deps('ad_hoc_utils', 'ml-dev')

import logging
import pytest
import pytest_rerunfailures

from pyexpect import expect
from hypothesis import given, settings, Phase, HealthCheck
import hypothesis.strategies as st
import hypothesis.extra.numpy as st_np
import hypothesis.extra.pandas as st_pd
from sklearn.datasets import make_classification

from time import time
import numpy as np
import pandas as pd
import operator as op

from functools import wraps
from ad_hoc_utils.common.test import st_seed, st_binary_problem, st_multiclass_problem, st_regression_problem
from ad_hoc_utils.common.f import (
    fanout, first, _c, _l, _r, _map, _sort, spread, _filter, L, select_keys, multimethod, fst, snd, named)

from ad_hoc_utils.common.algo import simulated_annealing
from ad_hoc_utils.ml.model_selection import (
    search_hyperparams, search_features,
    random_hps, random_mask,
    xgb_hyperparams_space, lgbm_hyperparams_space, cb_hyperparams_space)

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, f1_score, mean_squared_error
from xgboost import XGBClassifier, XGBRegressor
from lightgbm import LGBMClassifier, LGBMRegressor
from catboost import CatBoostClassifier, CatBoostRegressor


N_ATTEMPTS = 4
N_ITER = 10
N_JOBS = 16
N_BINS = 16
VERBOSE = False


default_settings = dict(
    phases = (Phase.generate,),
    deadline = None,
    suppress_health_check = [HealthCheck.function_scoped_fixture],
    derandomize = True)


def evaluator(f):
    def result(seed, data, *, problem_type, solver_type, mask = None, hps = dict()):
        (xs, ys) = data

        if mask is None:
            mask = np.ones(shape = xs.shape[1], dtype = bool)

        kwargs = dict(zip(
            ['xs_train', 'xs_test', 'ys_train', 'ys_test'],
            train_test_split(
                xs[:, mask], ys,
                train_size = 0.8,
                random_state = seed,
                stratify = ys if problem_type in {'binary', 'multiclass'} else None)))

        return f(seed = seed, hps = hps, **kwargs)
    result.__name__ = f.__name__
    return result


@multimethod(
    key = ('problem_type', 'solver_type'),
    name_to_key = _c(tuple, L.split('_')))
def evaluate(*args, problem_type, solver_type, **kwargs):
    pass


@evaluate.register
@evaluator
def evaluate_binary_xgb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = XGBClassifier(
        eval_metric = 'logloss',
        use_label_encoder = False,
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return roc_auc_score(ys_test, m.predict_proba(xs_test)[:, 1])


@evaluate.register
@evaluator
def evaluate_multiclass_xgb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = XGBClassifier(
        eval_metric = 'mlogloss',
        use_label_encoder = False,
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return f1_score(ys_test, m.predict(xs_test), average = 'macro')


@evaluate.register
@evaluator
def evaluate_regression_xgb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = XGBRegressor(
        eval_metric = 'rmse',
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return -mean_squared_error(ys_test, m.predict(xs_test))


@evaluate.register
@evaluator
def evaluate_binary_lgbm(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = LGBMClassifier(
        objective = 'binary',
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return roc_auc_score(ys_test, m.predict_proba(xs_test)[:, 1])


@evaluate.register
@evaluator
def evaluate_multiclass_lgbm(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = LGBMClassifier(
        objective = 'mutliclass',
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return f1_score(ys_test, m.predict(xs_test), average = 'macro')


@evaluate.register
@evaluator
def evaluate_regression_lgbm(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = LGBMRegressor(
        objective = 'regression',
        n_jobs = N_JOBS,
        random_state = seed,
        **hps)
    m.fit(xs_train, ys_train)
    return -mean_squared_error(ys_test, m.predict(xs_test))


def quantify(xs, n_bins):
    return np.vstack([
        np.searchsorted(
            np.quantile(xs[:, i], np.linspace(0, 1, 1 + n_bins)[1:-1]),
            xs[:, i])
        for i in range(xs.shape[1])]).T


@evaluate.register
@evaluator
def evaluate_binary_cb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = CatBoostClassifier(
        loss_function = 'Logloss',
        thread_count = N_JOBS,
        random_state = seed,
        verbose = False,
        **hps)

    m.fit(quantify(xs_train, N_BINS), ys_train)
    return roc_auc_score(ys_test, m.predict_proba(xs_test)[:, 1])


@evaluate.register
@evaluator
def evaluate_multiclass_cb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = CatBoostClassifier(
        loss_function = 'MultiClass',
        thread_count = N_JOBS,
        random_state = seed,
        verbose = False,
        **{k:v for k, v in hps.items() if k not in {'subsample'}})
    m.fit(quantify(xs_train, N_BINS), ys_train)
    return f1_score(ys_test, m.predict(xs_test), average = 'macro')


@evaluate.register
@evaluator
def evaluate_regression_cb(seed, xs_train, xs_test, ys_train, ys_test, hps):
    m = CatBoostRegressor(
        loss_function = 'MAE',
        thread_count = N_JOBS,
        random_state = seed,
        verbose = False,
        **hps)
    m.fit(quantify(xs_train, N_BINS), ys_train)
    return -mean_squared_error(ys_test, m.predict(xs_test))

strategies = dict(
    binary = st_binary_problem,
    multiclass = st_multiclass_problem,
    regression = st_regression_problem)


strategy_params = dict(
    hps = dict(),
    fs = dict(st_n_features = st.integers(min_value = 4, max_value = 10)))


spaces = dict(
    xgb = xgb_hyperparams_space,
    lgbm = lgbm_hyperparams_space,
    cb = cb_hyperparams_space)


methods = {
    k: _c(list, _map(lambda args: pytest.param(*args, id = args[0])))(v)
    for k, v in dict(
        hps = [
            ('random', dict()),
            ('hyperopt', dict()),
            ('bayesian', dict()),
            ('genetic', dict()),
            ('simulated_annealing', dict())],
        fs  = [
            ('random', dict(n_iter = N_ITER, n_features = 3)),
            ('simulated_annealing', dict(n_iter = N_ITER)),
            ('dynamic', dict(n_features = 4, depth = 2)),
            ('backward_selection', dict(max_q_lose = 1.0)),
            ('backward_selection', dict(n_features = 4)),
            ('forward_selection_by_quality', dict(min_q_improve = 1.0)),
            ('forward_selection_by_n_features', dict(n_features = 4))]).items()}


handlers = dict(
    hps = dict(
        eval_adapter = lambda f: lambda hps: f(hps = hps),
        baseline = lambda params, data, solution, rng: random_hps(rng, spaces[params['solver_type']]),
        search = search_hyperparams,
        extra_params = lambda params, data: dict(
            space = spaces[params['solver_type']],
            n_iter = params['n_iter'])),

    fs = dict(
        eval_adapter = lambda f: lambda mask: f(mask = mask),
        baseline = lambda parms, data, solution, rng: random_mask(rng, data[0].shape[1], int(solution.sum())),
        search = search_features,
        extra_params = lambda params, data: dict(n_columns = data[0].shape[1])))


def run_test(params, data, seed):
    h = handlers[params['kind']]

    evaluate_ = h['eval_adapter'](_r(
        evaluate,
        seed, data,
        **select_keys(params, ['solver_type', 'problem_type'])))

    t0 = time()
    solution = h['search'](
        evaluate_,
        method = params['method'],
        seed = seed,
        **{**h['extra_params'](params, data), **params['method_kwargs']},
        verbose = VERBOSE)
    dt = time() - t0
    best_result = evaluate_(solution)

    rng = np.random.default_rng(seed)
    baseline_result = _c(L.mean(), np.array, list, _map(evaluate_))([
        h['baseline'](params, data, solution, rng) for _ in range(params['n_iter'])])

    logging.info(f'{baseline_result:0.4f} -> {best_result:0.4f} in {dt:0.2f}s')
    expect(best_result).to_be.greater_or_equal(baseline_result - 1e-12)


def mk_test(kind, problem_type, **extra_settings):
    @pytest.mark.flaky(reruns = N_ATTEMPTS)
    @settings(**{**default_settings, **extra_settings})
    @pytest.mark.parametrize(
        'method,method_kwargs',
        methods[kind])
    @pytest.mark.parametrize(
        'solver_type',
        _c(list, _map(snd), _filter(L == problem_type, fst))(evaluate._handlers.keys()))
    @given(strategies[problem_type](**strategy_params[kind]))
    @_l(named, f'test_{kind}_{problem_type}')
    def result(method, method_kwargs, solver_type, seed, data):
        run_test(dict(
            kind = kind,
            problem_type = problem_type,
            method = method,
            method_kwargs = method_kwargs,
            solver_type = solver_type,
            n_iter = N_ITER), data, seed)
    return result


def  __init__(g):
    for kind in ['hps', 'fs']:
        for problem_type in strategies.keys():
            f = mk_test(kind, problem_type, max_examples = 1)
            g[f.__name__] = f

__init__(globals())
