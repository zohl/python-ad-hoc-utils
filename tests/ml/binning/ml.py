from ad_hoc_utils.common.imp import extras

extras.require_deps('ad_hoc_utils', 'ml-dev')


import pytest
from pyexpect import expect
from hypothesis import given
import hypothesis.strategies as st
import hypothesis.extra.numpy as st_np
import hypothesis.extra.pandas as st_pd
from sklearn.datasets import make_classification

import numpy as np
import pandas as pd
import operator as op

from ad_hoc_utils.common.test import st_seed
from ad_hoc_utils.common.f import fanout, first, _c, _l, _r, _map, L, _map, _sort, spread, _filter, select_keys
from ad_hoc_utils.common.algo import simulated_annealing
from ad_hoc_utils.ml.binning import mk_bins, merge_bins, bins_proximity, bin_split, bin_group, bin_separate_nan


mk_data = lambda n_samples, random_state, weight: _c(
    first(L.reshape(-1)), fanout(L[0], L[1]), make_classification)(
    n_samples = n_samples, random_state = random_state, weights = (weight,),
    n_features = 1, n_informative = 1, n_redundant = 0,
    n_classes = 2, n_clusters_per_class = 1)


st_size = lambda max_value = 100: st.integers(min_value = 0, max_value = max_value)
st_weight = lambda: st.floats(min_value = 0.0, max_value = 1.0, allow_nan = False, allow_infinity = False)


filter_regular_bins = _c(list, _filter(op.not_, np.isnan, L['l']))
filter_nan_bin = _c(list, _map(_r(select_keys, ['n_total', 'n_event'])), _filter(np.isnan, L['l']))


sample_data = (
    [1, 2, 3, 4, 5, 6],
    [0, 1, 1, 0, 1, 0])



@pytest.mark.parametrize('xs,ys,result', [
    ([1, 2, 3, 4],
     None,
     [{'l': 1, 'r': 1, 'n_total': 1},
      {'l': 2, 'r': 2, 'n_total': 1},
      {'l': 3, 'r': 3, 'n_total': 1},
      {'l': 4, 'r': 4, 'n_total': 1}]),

    ([1, 2, 3, 4],
     [1, 0, 1, 1],
     [{'l': 1, 'r': 1, 'n_total': 1, 'n_events': 1},
      {'l': 2, 'r': 2, 'n_total': 1, 'n_events': 0},
      {'l': 3, 'r': 3, 'n_total': 1, 'n_events': 1},
      {'l': 4, 'r': 4, 'n_total': 1, 'n_events': 1}]),

    ([1, 4, 6, 9],
     [1, 0, 1, 1],
     [{'l': 1, 'r': 1, 'n_total': 1, 'n_events': 1},
      {'l': 4, 'r': 4, 'n_total': 1, 'n_events': 0},
      {'l': 6, 'r': 6, 'n_total': 1, 'n_events': 1},
      {'l': 9, 'r': 9, 'n_total': 1, 'n_events': 1}]),

    ([-4.5, -2.34, 1.89, 9.234],
     [1, 0, 1, 1],
     [{'l': -4.5, 'r': -4.5, 'n_total': 1, 'n_events': 1},
      {'l': -2.34, 'r': -2.34, 'n_total': 1, 'n_events': 0},
      {'l': 1.89, 'r': 1.89, 'n_total': 1, 'n_events': 1},
      {'l': 9.234, 'r': 9.234, 'n_total': 1, 'n_events': 1}])])
def test_mk_bins_singleton_simple(xs, ys, result):
    expect(mk_bins(xs, ys, method = 'singleton')).to_equal(result)


@given(st.builds(mk_data, n_samples = st_size(), random_state = st_seed(), weight = st_weight()))
def test_mk_bins_singleton_generic(data):
    (xs, ys) = data

    bs = mk_bins(xs, ys, method = 'singleton')

    expect(len(bs)).to_equal(len(xs))
    expect(_c(set, _map(L['l']))(bs)).to_equal(set(xs))
    expect(_c(set, _map(L['r']))(bs)).to_equal(set(xs))

    if len(xs) > 0:
        expect(_c(sum, _map(L['n_total']))(bs)).to_equal(len(xs))
        expect(_c(sum, _map(L['n_events']))(bs)).to_equal(sum(ys))

        expect(_c(set, _map(L['n_total']))(bs)).to_equal({1})



@pytest.mark.parametrize('xs,ys,n_bins,result', [
    ([1, 2, 3, 4],
     [1, 0, 1, 1],
     2,
     [{'l': 1, 'r': 2, 'n_total': 2, 'n_events': 1},
      {'l': 3, 'r': 4, 'n_total': 2, 'n_events': 2}]),

    ([1, np.nan, 2, 3],
     [1, 0, 1, 1],
     2,
     [{'l': np.nan, 'r': np.nan, 'n_total': 1, 'n_events': 0},
      {'l': 1.0, 'r': 2.0, 'n_total': 2, 'n_events': 2},
      {'l': 3.0, 'r': 3.0, 'n_total': 1, 'n_events': 1}]),

    ([1, 2, 3, 4],
     [1, 0, 1, 1],
     4,
     [{'l': 1, 'r': 1, 'n_total': 1, 'n_events': 1},
      {'l': 2, 'r': 2, 'n_total': 1, 'n_events': 0},
      {'l': 3, 'r': 3, 'n_total': 1, 'n_events': 1},
      {'l': 4, 'r': 4, 'n_total': 1, 'n_events': 1}]),

    ([1, 2, 3, 9],
     [1, 0, 1, 1],
     2,
     [{'l': 1, 'r': 3, 'n_total': 3, 'n_events': 2},
      {'l': 9, 'r': 9, 'n_total': 1, 'n_events': 1}]),

    ([1, 2, 3, 9],
     [1, 0, 1, 1],
     3,
     [{'l': 1, 'r': 3, 'n_total': 3, 'n_events': 2},
      {'l': 9, 'r': 9, 'n_total': 1, 'n_events': 1}])])
def test_mk_bins_equiwidth_simple(xs, ys, n_bins, result):
    bs = mk_bins(xs, ys, n_bins = n_bins, method = 'equiwidth')
    expect(filter_regular_bins(bs)).to_equal(filter_regular_bins(result))
    expect(filter_nan_bin(bs)).to_equal(filter_nan_bin(result))


@given(st_size().flatmap(lambda size: st.tuples(
    st.integers(min_value = 1, max_value = max(1, size // 2)),
    st.builds(
        _r(mk_data, n_samples = size),
        random_state = st_seed(),
        weight = st_weight()))))
def test_mk_bins_equiwidth_generic(case):
    (n_bins, (xs, ys)) = case

    bs = mk_bins(xs, ys, n_bins = n_bins, method = 'equiwidth')

    if len(bs) > 0:
        expect(_c(sum, _map(L['n_total']))(bs)).to_equal(len(xs))
        expect(_c(sum, _map(L['n_events']))(bs)).to_equal(sum(ys))

        expect(len(bs)).to_be.less_or_equal(n_bins)

        if len(bs) > 1:
            w = (np.max(xs) - np.min(xs))/n_bins
            ds_max = _c(max, _map(lambda x: (x['r'] - x['l'])))(bs)
            expect(ds_max).to_be.less_or_equal(w)


@pytest.mark.parametrize('xs,ys,n_bins,result', [
    ([1, 2, 3, 4],
     [1, 0, 1, 1],
     2,
     [{'l': 1, 'r': 2, 'n_total': 2, 'n_events': 1},
      {'l': 3, 'r': 4, 'n_total': 2, 'n_events': 2}]),

    ([1, 2, 2, 3],
     [1, 0, 1, 1],
     2,
     [{'l': 1, 'r': 2, 'n_total': 2, 'n_events': 1},
      {'l': 2, 'r': 3, 'n_total': 2, 'n_events': 2}])])
def test_mk_bins_equisized_simple(xs, ys, n_bins, result):
    pass


@given(st_size().flatmap(lambda size: st.tuples(
    st.integers(min_value = 1, max_value = max(1, size // 2)),
    st.builds(
        _r(mk_data, n_samples = size),
        random_state = st_seed(),
        weight = st_weight()))))
def test_mk_bins_equisized_generic(case):
    (n_bins, (xs, ys)) = case

    bs = mk_bins(xs, ys, n_bins = n_bins, method = 'equisized')

    if len(xs) > 0:
        expect(_c(sum, _map(L['n_total']))(bs)).to_equal(len(xs))
        expect(_c(sum, _map(L['n_events']))(bs)).to_equal(sum(ys))

        expect(len(bs)).to_equal(n_bins)

        lens = _c(set, _map(L['n_total']))(bs)
        expect(max(lens) - min(lens)).to_be.less_or_equal(1)
    else:
        expect(bs).to_equal([])


@pytest.mark.parametrize('xs,ys,result', [
    ([1, 2, 3, 4, 5, 6],
     [0, 0, 1, 1, 1, 0],
     [{'l': 1, 'r': 2, 'n_total': 2, 'n_events': 0},
      {'l': 3, 'r': 5, 'n_total': 3, 'n_events': 3},
      {'l': 6, 'r': 6, 'n_total': 1, 'n_events': 0}]),

    ([1, 2, 3, 4, 5, 6],
     [1, 0, 1, 1, 0, 0],
     [{'l': 1, 'r': 2, 'n_total': 2, 'n_events': 1},
      {'l': 3, 'r': 4, 'n_total': 2, 'n_events': 2},
      {'l': 5, 'r': 6, 'n_total': 2, 'n_events': 0}])])
def test_mk_bins_cart_simple(xs, ys, result):
    expect(mk_bins(xs, ys, method = 'cart')).to_equal(result)


@given(st.builds(
    mk_data,
    n_samples = st_size(),
    random_state = st_seed(),
    weight = st_weight()))
def test_mk_bins_cart_generic(data):
    (xs, ys) = data
    bs = mk_bins(xs, ys, method = 'cart')
    if len(bs) > 0:
        expect(_c(sum, _map(L['n_total']))(bs)).to_equal(len(xs))
        expect(_c(sum, _map(L['n_events']))(bs)).to_equal(sum(ys))


@pytest.mark.parametrize('xs,ys,result', [
    ([1, 2, 3, 4, 5, 6],
     [0, 0, 1, 1, 1, 0],
     [{'l': 3, 'r': 6, 'n_total': 4, 'n_events': 3},
      {'l': 1, 'r': 2, 'n_total': 2, 'n_events': 0}]),

    ([1, 2, 3, 4, 5, 6],
     [1, 0, 1, 1, 0, 0],
     [{'l': 1, 'r': 6, 'n_total': 6, 'n_events': 3}])])
def test_mk_bins_woe_simple(xs, ys, result):
    expect(mk_bins(xs, ys, method = 'woe', min_size = 1, p_threshold = 0.05)).to_equal(result)


@given(st.builds(
    mk_data,
    n_samples = st_size(),
    random_state = st_seed(),
    weight = st_weight()))
def test_mk_bins_woe_generic(data):
    (xs, ys) = data

    p_threshold = 0.05
    min_size = min(20, len(xs))

    bs = mk_bins(xs, ys, min_size = min_size, p_threshold = p_threshold, method = 'woe')
    if len(bs) > 0:
        expect(_c(sum, _map(L['n_total']))(bs)).to_equal(len(xs))
        expect(_c(sum, _map(L['n_events']))(bs)).to_equal(sum(ys))

        if len(bs) > 1:
            expect(bs[0]['n_events']/bs[0]['n_total']).to_be.greater_or_equal(bs[-1]['n_events']/bs[-1]['n_total'])


def _test_merge_bins_concat(bs0):
    bs = merge_bins(bs0, method = 'concat')
    if len(bs) > 0:
        expect(_c(min, _map(L['l']))(bs0)).to_equal(_c(min, _map(L['l']))(bs))
        expect(_c(max, _map(L['r']))(bs0)).to_equal(_c(max, _map(L['r']))(bs))

        expect(len(bs)).to_equal(1)


def test_merge_bins_concat_simple():
    (xs, ys) = sample_data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_concat(bs)


@given(st.builds(mk_data, n_samples = st_size(), random_state = st_seed(), weight = st_weight()))
def test_merge_bins_concat_generic(data):
    (xs, ys) = data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_concat(bs)


def _test_merge_bins_event_rate(bs0):
    bs = merge_bins(bs0, method = 'event_rate')
    if len(bs) > 0:
        expect(_c(min, _map(L['l']))(bs0)).to_equal(_c(min, _map(L['l']))(bs))
        expect(_c(max, _map(L['r']))(bs0)).to_equal(_c(max, _map(L['r']))(bs))

        rs = _c(list, _map(lambda x: x['n_events']/x['n_total']))(bs)
        expect(_c(all, _map(spread(op.ge)), zip)(rs, rs[1:])).to_equal(True)


def test_merge_bins_event_rate_simple():
    (xs, ys) = sample_data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_event_rate(bs)


@given(st.builds(mk_data, n_samples = st_size(), random_state = st_seed(), weight = st_weight()))
def test_merge_bins_event_rate_generic(data):
    (xs, ys) = data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_event_rate(bs)


def _test_merge_bins_proximity(bs0):
    p_threshold = 0.05
    min_size = min(20, _c(sum, _map(L['n_total']))(bs0))

    bs = merge_bins(bs0, min_size = min_size, p_threshold = p_threshold, method = 'proximity')

    if len(bs) > 0:
        expect(_c(min, _map(L['l']))(bs0)).to_equal(_c(min, _map(L['l']))(bs))
        expect(_c(max, _map(L['r']))(bs0)).to_equal(_c(max, _map(L['r']))(bs))

        expect(_c(all, _map(L < p_threshold, spread(bins_proximity)), zip)(bs, bs[1:])).to_equal(True)
        expect(_c(all, _map(L >= min_size, L['n_total']))(bs)).to_equal(True)


def test_merge_bins_proximity_simple():
    (xs, ys) = sample_data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_proximity(bs)


@given(st.builds(mk_data, n_samples = st_size(), random_state = st_seed(), weight = st_weight()))
def test_merge_bins_proximity_generic(data):
    (xs, ys) = data
    bs = mk_bins(xs, ys, method = 'singleton')
    _test_merge_bins_proximity(bs)



@st.composite
def st_integer_sample(draw, min_size, max_size, l = 1):
    rng = np.random.default_rng(seed = draw(st_seed()))
    xs = rng.poisson(l, draw(st.integers(min_value = min_size, max_value = max_size)))
    vs0 = list(set(xs))
    vs1 = draw(st.permutations(range(len(vs0))))
    mapping = dict(zip(vs0, vs1))

    return np.array([mapping[x] for x in xs])

def _eval_bins(bs, xs, n_bins):
    rs = _c(np.array, list, _map(L - (len(xs)/n_bins), float, L['n_total']))(bs)
    return rs.dot(rs)


@mk_bins.register
@bin_separate_nan
@bin_split
@bin_group
def mk_bins_dynamic_sa(xs, ys = None, *, method, n_bins, seed = None, **kwargs):
    if n_bins > len(xs):
        n_bins = len(vs)

    target_mean = sum(xs)/n_bins

    def t_evaluate(s):
        ls = []
        for i, j in zip([0, *s], [*s, len(xs)]):
            ls.append(sum(xs[i:j]))
        rs = np.array(ls) - target_mean

        return rs.dot(rs)


    def t_mutate(s, rng, t):
        mutations = [
            *[(i, 1)
              for i, (l, r) in enumerate(zip(s, [*s[1:], len(xs)]))
              if r - l > 1],
            *[(i, -1)
              for i, (l, r) in enumerate(zip([0, *s], s))
              if r - l > 1]]

        if len(mutations) == 0:
            return s

        i, d = rng.choice(mutations)
        return [*s[:i], s[i] + d, *s[i+1:]]

    rng = np.random.default_rng(seed = seed)
    s0 = list(sorted(rng.choice(range(1, n_bins), size = n_bins - 1, replace = False)))
    s =  simulated_annealing(s0, t_evaluate, t_mutate, seed = seed, **kwargs)

    return s


@given(st_integer_sample(2, 300, l = 5).filter(lambda xs: len(np.unique(xs)) > 1).flatmap(lambda xs: st.tuples(
        st.just(xs),
        st.integers(min_value = 2, max_value = len(np.unique(xs))))))
def test_mk_bins_dynamic_against_quantile(case):
    (xs, n_bins) = case

    bs0 = mk_bins(xs, n_bins = n_bins, method = 'dynamic')
    bs1 = mk_bins(xs, n_bins = n_bins, method = 'quantile')

    if len(bs1) == n_bins:
        expect(_eval_bins(bs0, xs, n_bins)).to_be.less_or_equal(_eval_bins(bs1, xs, n_bins))


@given(st_integer_sample(2, 300, l = 5).filter(lambda xs: len(np.unique(xs)) > 1).flatmap(lambda xs: st.tuples(
        st.just(xs),
        st.integers(min_value = 2, max_value = len(np.unique(xs))))),
       st_seed())
def test_mk_bins_dynamic_against_sa(case, seed):
    (xs, n_bins) = case

    bs0 = mk_bins(xs, n_bins = n_bins, method = 'dynamic')
    bs1 = mk_bins(xs, n_bins = n_bins, method = 'dynamic_sa', n_iter = 300, seed = seed)

    expect(_eval_bins(bs0, xs, n_bins)).to_be.less_or_equal(1e-12 + _eval_bins(bs1, xs, n_bins))
