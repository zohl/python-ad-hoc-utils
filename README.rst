Ad hoc utils
============

A collection of miscellaneous functions and code snippets that are too small to have a separate library and not so small to re-implement them again.
