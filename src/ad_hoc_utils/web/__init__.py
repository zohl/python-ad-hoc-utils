from typing import Any, Callable

from dominate.util import raw
from functools import partial
from functools import wraps
from hypercorn.asyncio import serve
from hypercorn.config import Config
from pathlib import Path
from pathlib import Path
from quart import (
    Quart, jsonify,
    request, g, current_app,
    send_from_directory, Blueprint, render_template_string,
    testing)
from quart_cors import cors
from werkzeug.datastructures import Headers
from sqlalchemy import MetaData
import argparse
import asyncio
import dominate
import dominate.tags as dom
import importlib
import logging
import sqlalchemy as sql
import yaml
import warnings

from ad_hoc_utils.common.config import from_env
from ad_hoc_utils.common.db import connect_to_db
from ad_hoc_utils.common.f import _c, _r, L, P, get_in
from ad_hoc_utils.common.f import select_keys
from ad_hoc_utils.common.f import _l, _r, named


class CustomQuartClient(testing.QuartClient):

    def __init__(self, app, use_cookies = True, headers = {}):
        self._headers = Headers(headers)
        super().__init__(app, use_cookies)

    def open(self, *args, **kwargs):
        headers = self._headers.copy()
        headers.extend(kwargs.pop('headers', Headers()))
        kwargs['headers'] = headers

        return super().open(*args, **kwargs)


async def teardown_db():
    if 'db' in current_app.extensions:
        del current_app.extensions['md']

        db = current_app.extensions['db']
        db.dispose()

        del current_app.extensions['db']


def get_db():
    if 'db' not in current_app.extensions:
        db_cfg = current_app.user_config['db']

        logging.info(f"getting db: {db_cfg['db_type']}, {db_cfg['user']}")

        # TODO: generalize it
        db = connect_to_db(
            db_type = 'postgres',
            host = db_cfg['host'],
            db_name = db_cfg['name'],
            user = db_cfg['user'],

            # TODO: string as a parameter
            password = from_env('DB_{db_type}_{user}'))

        md = MetaData(schema = db_cfg['schema'])
        with warnings.catch_warnings():
            warnings.filterwarnings(
                action = 'ignore',
                category = sql.exc.SAWarning,
                message = '^Skipped unsupported reflection of expression-based index .*')
            md.reflect(db)

        current_app.extensions['db'] = db
        current_app.extensions['md'] = md
        current_app.on_teardown_callbacks.append(teardown_db)

    return current_app.extensions['db']


def get_db_table(table_name):
    get_db()
    md = current_app.extensions['md']
    return md.tables['.'.join([md.schema, table_name])]


def db_get_or_create(tx, tbl, where, insert = dict(), select = []):

    select_clause = tbl.c.values() if len(select) == 0 else [tbl.c[k] for k in select]
    where_clause = [(tbl.c[k] == v) for k, v in where.items()]

    r = tx.execute(
        sql
        .select(*select_clause)
        .where(*where_clause)).mappings().fetchone()

    if r is None:
        r = tx.execute(
            tbl
            .insert({**where, **insert})
            .returning(*select_clause)).mappings().fetchone()

    return r


def with_tx(f):
   @wraps(f)
   async def result(*args, **kwargs):
       with get_db().begin() as tx:
           return await f(tx, *args, **kwargs)
   return result


def with_json_body(f):
    @wraps(f)
    async def result(*args, **kwargs):
        body = (await request.get_json()) or dict()
        return await f(body = body, *args, **kwargs)
    return result


def with_form_body(f):
    @wraps(f)
    async def result(*args, **kwargs):
        body = (await request.form) or dict()
        return await f(body = body, *args, **kwargs)
    return result


def mk_html_template(*args, **kwargs):
    html = dominate.document(*args, **kwargs)

    with html.head:
        dom.meta(charset = 'utf-8')
        dom.meta(
            name = 'viewport',
            content = 'width=device-width, initial-scale=1')
        dom.link(
            href = '/static/bootstrap.min.css',
            rel = 'stylesheet',
            integrity = 'sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor')
        dom.link(href = '/static/style.css', rel = 'stylesheet')

    with html:
        dom.script(
            src = '/static/bootstrap.bundle.min.js',
            integrity = 'sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2')

    return html



UserConfig = dict[str, Any]


load_config: Callable[[str | Path], UserConfig] = _c(
    _r(yaml.load, Loader = yaml.Loader), L.open('r'), Path)



def _send_from_directory(d):
    async def result(path):
        return await send_from_directory(d, path)
    return result


def mk_blueprint_static(config: UserConfig):
    bp = Blueprint('static', 'static')

    for k, v in config['backend']['static'].items():
        d = config['directories'][v]
        logging.info(f'registering static dir: {k} -> {d}')

        bp.route(f'/{k}/<path:path>', methods = ['GET'])(
            named(f'static_{k}_get', _send_from_directory(d)))

    return bp


async def create_app(config: UserConfig) -> Quart:
    app = cors(
        Quart(
            __name__,
            static_url_path = None,
            static_folder = None),
        allow_origin  = "*",
        allow_headers = ["authorization", "content-type"],
        allow_methods = ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"])

    app.user_config = config
    app.on_teardown_callbacks = []

    async with app.app_context():
        for module_name in config['modules']:
            m = importlib.import_module(module_name)
            app.register_blueprint(getattr(m, 'mk_blueprint')(config))

    return app


async def teardown_app(app: Quart) -> None:
    async with app.app_context():
        for f in current_app.on_teardown_callbacks:
            await f()


def resolve_dirs(config: UserConfig, cwd: str | Path = '.', **kwargs) -> None:
    if 'directories' in config:
        cfg_dirs = config['directories']
        resolved = set()

        for k in cfg_dirs.keys():
            v = Path(config['directories'][k].format(
                **kwargs, **{k: str(v) for k, v in cfg_dirs.items() if k in resolved}))
            v.mkdir(parents = True, exist_ok = True)
            cfg_dirs[k] = v.absolute()
            resolved.add(k)

        cfg_dirs['cwd'] = Path(cwd).absolute()


async def start_server():
    logging.basicConfig(
        format = '%(asctime)s %(levelname)s %(message)s',
        datefmt = '%Y.%m.%d %H:%M:%S',
        level = logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', '-c', type = str, required = True, help = 'config file to use')
    parser.add_argument('--root-dir','-r', type = str, required = False, help = 'path to project root directory')
    args = parser.parse_args()

    config_path = args.config
    config = load_config(config_path)
    logging.info(f'loaded config: {config}')

    app = await create_app(config)
    resolve_dirs(config, cwd = Path(config_path).parent, root = args.root_dir)

    is_debug = (get_in(config, *map(L.get, ['backend', 'debug'])) or False)
    port = get_in(config, *map(L.get, ['backend', 'port']))

    try:
        if is_debug:
            logging.getLogger().setLevel(logging.DEBUG)
            await app.run_task(
                debug = True,
                host = '0.0.0.0',
                port = port)
        else:
            await serve(app, Config.from_mapping(bind = f'0.0.0.0:{port}'))
    finally:
        await teardown_app(app)



__all__ = [
    'get_db', 'get_db_table', 'teardown_db',
    'db_get_or_create',
    'with_tx',
    'with_json_body',
    'with_form_body',
    'mk_html_template',
    'mk_blueprint_static',
    'load_config', 'resolve_dirs',
    'create_app', 'teardown_app',
    'start_server']
