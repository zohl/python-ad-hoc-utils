import numpy as np


def iv(n_pos, n_neg):
    eps = 1e-12
    d_pos = n_pos/n_pos.sum()
    d_neg = n_neg/n_neg.sum()

    return (d_pos - d_neg).dot(np.log((d_pos + eps)/(d_neg + eps)))


__all__ = [
    'iv']
