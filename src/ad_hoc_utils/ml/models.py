import hashlib

from xgboost import XGBClassifier, XGBRegressor
from lightgbm import LGBMClassifier

from ad_hoc_utils.common.f import _c, L, multimethod


encode_name = _c(L.hexdigest(), hashlib.md5, L.encode('utf8'), str)

@multimethod(key = 'type')
def fit(type, *args, **kwargs):
    '''Fits model. '''

@multimethod(key = 'type')
def predict(type, model, *args, **kwargs):
    '''Scores data using model.'''

@fit.register
def fit_xgb_classifier(type, hps, xs, ys):
    model = XGBClassifier(**hps)
    model.fit(xs.rename(encode_name, axis = 1), ys)
    return model

@predict.register
def predict_xgb_classifier(type, model, xs):
    return model.predict_proba(xs.rename(encode_name, axis = 1))[:, 1]


@fit.register
def fit_xgb_regressor(type, hps, xs, ys):
    model = XGBRegressor(**hps)
    model.fit(xs.rename(encode_name, axis = 1), ys)
    return model

@predict.register
def predict_xgb_regressor(type, model, xs):
    return model.predict(xs.rename(encode_name, axis = 1))


@fit.register
def fit_lgbm_classifier(type, hps, xs, ys):
    model = LGBMClassifier(**(dict(importance_type = 'gain') | hps))
    model.fit(xs.rename(encode_name, axis = 1), ys)
    return model


@predict.register
def predict_lgbm_classifier(type, model, xs):
    return model.predict_proba(xs.rename(encode_name, axis = 1))[:, 1]


@multimethod(key = 'type')
def save_model(type, model, fn, **kwargs):
    pass

@multimethod(key = 'type')
def load_model(type, fn, **kwargs):
    pass

@save_model.register
def save_model_xgb_classifier(model, fn):
    model.save_model(fn)

@load_model.register
def load_model_xgb_classifier(fn):
    model = XGBClassifier()
    model.load_model(fn)
    return model

@save_model.register
def save_model_xgb_regressor(model, fn):
    model.save_model(fn)

@load_model.register
def load_model_xgb_regressor(fn):
    model = XGBRegressor()
    model.load_model(fn)
    return model

__all__ = ['fit', 'predict', 'encode_name']
