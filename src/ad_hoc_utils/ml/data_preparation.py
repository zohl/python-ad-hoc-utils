from tqdm.auto import tqdm
import pandas as pd
import numpy as np
from pathlib import Path
import fastparquet as fp
from itertools import chain

from ad_hoc_utils.common.f import *
from ad_hoc_utils.common.db import *
from sqlalchemy import MetaData, Table, Column

import logging
import operator as op
import json

from pathlib import Path
from tqdm import tqdm

from ad_hoc_utils.ml.feature_extraction import extract
# TODO: also use from tqdm.notebook import tqdm

from ad_hoc_utils.common.f import (
    _c, _l, _r, _map, _filter, _sort, fst, snd, L)


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


idx_to_labels = _c(list, _map(fst), _filter(snd), L.items())


def df_to_parquet(df, fn, chunk_size = 250 * 1024 * 1024):
    row_size = df.memory_usage().sum()/df.shape[0]
    n_rows = int(chunk_size / row_size)
    n_chunks = (df.shape[0] + n_rows - 1)//n_rows

    if n_chunks == 1:
        df.to_parquet(fn)
    else:
        fn_prefix = _c('.'.join, L[:-1], L.split('.'), str)(fn)

        for i in range(n_chunks):
            df.iloc[i*n_rows:(i+1)*n_rows].to_parquet(f'{fn_prefix}.{i:03}')
        Path(fn).touch()


list_parquet_chunks = lambda fn: _c(
    _sort(),
    _filter(op.not_, L.endswith('.parquet'), str),
    fn.parent.glob)(fn.stem + '.*')


def df_from_parquet(fn, columns = None, verbose = True):
    fn = Path(fn)
    is_singleton = (fn.stat().st_size > 0)

    if columns is not None:
        available_columns = _c(set, P.columns, fp.ParquetFile)(
            fn
            if is_singleton
            else _c(next, iter, list_parquet_chunks)(fn))

        columns = [c for c in columns if c in available_columns]

    read_parquet = _r(_r(pd.read_parquet, columns = columns))

    return (
        read_parquet(fn)
        if is_singleton
        else _c(
            _r(pd.concat, axis = 0),
            _map(read_parquet),
            tqdm if verbose else identity, list,
            list_parquet_chunks)(fn))


@multimethod(key = lambda datasource: datasource['type'])
def fetch_data(datasource, source_name, params):
    '''
    Datasource:
    - type :: str
    '''
    pass


@fetch_data.register
def fetch_data_file(datasource, source_name, params = dict()):
    '''
    Datasource:
    - base_dir :: str

    Params:
    - min_snapshot_dt :: datetime.datetime -- filter for chunked dataframe
    - max_snapshot_dt :: datetime.datetime -- filter for chunked dataframe
    - snapshot_dt_col :: str | None -- snapshot_dt column
    - base            :: pd.DataFrame | None -- primary key filter
    - columns         :: [str] | None -- columns to load (all of ``None``)
    '''

    basename = source_name.format(**params)

    df_base = params.get('base')

    snapshot_dt_col = params.get('snapshot_dt_col')

    min_snapshot_dt = params.get(
        'min_snapshot_dt',
        None
        if (df_base is None) or (snapshot_dt_col is None)
        else df_base[snapshot_dt_col].min())

    max_snapshot_dt = params.get(
        'max_snapshot_dt',
        None
        if (df_base is None) or (snapshot_dt_col is None)
        else df_base[snapshot_dt_col].max())

    required_cols = _c(list, set)([
        *([] if df_base is None else list(df_base.columns)),
        snapshot_dt_col])

    load_df = _c(
        # restriction by ``base``
        (identity
         if df_base is None
         else _r(df_base.merge, on = list(df_base.columns), how = 'inner')),


        # restriction by ``min_snapshot_dt`` and ``max_snapshot_dt``
        (identity
         if snapshot_dt_col is None
         else _c(L.copy(),
                 (identity if min_snapshot_dt is None else lambda df: df[df[snapshot_dt_col] >= min_snapshot_dt]),
                 (identity if max_snapshot_dt is None else lambda df: df[df[snapshot_dt_col] <= max_snapshot_dt]))),

        _r(df_from_parquet, columns = (
            None
            if params.get('columns') is None
            else _c(list, set, chain)(required_cols, params.get('columns')))))


    fn = Path(datasource['base_dir']) / (basename + '.parquet')
    if fn.exists():
        return load_df(fn)

    fn = Path(datasource['base_dir']) / basename
    if fn.exists():
        return _c(
            _r(pd.concat, axis = 0), list, _map(load_df), tqdm, list,

            (identity
             if min_snapshot_dt is None
             else _filter(lambda x: x.stem >= min_snapshot_dt.strftime('%Y-%m-%d')[:len(x.stem)])),

            (identity
             if max_snapshot_dt is None
             else _filter(lambda x: x.stem <= max_snapshot_dt.strftime('%Y-%m-%d')[:len(x.stem)])),

            L.glob('*.parquet'))(fn)

    raise Exception(f'`{basename}` is not found')


@fetch_data.register
def fetch_data_sql(datasource, source_name, params = dict()):
    '''
    Datasource:
    - queries_dir :: str
    - db :: Engine -- database engine (a separate transaction per item)
    - tx :: Connection -- active transaction
    - tx :: MetaData -- SQLAchemy's metadata associated with the engine
    '''

    query = ((Path(datasource['queries_dir']) / (source_name + '.sql'))
            .read_text()
            .format(**{**datasource, **params}))

    if 'tx' in datasource:
        return sql_load_to_pandas(datasource['tx'], query, params)

    if 'db' in datasource:
        with datasource['db'].begin() as tx:
            return sql_load_to_pandas(tx, query, params)

    raise Exception(f'Neither `tx` or `db` provided')


@multimethod(key = 'type')
def mk_datasource(root_dir, get_password, name, type, **kwargs):
    '''Creates datasource from config.'''


@mk_datasource.register
def mk_datasource_sql(
        root_dir, get_password, name, *,
        db_type, schema_name, base_table_name, global_params, **kwargs):

    db = connect_to_db(db_type, **kwargs, password = get_password)
    md = MetaData(schema = schema_name, bind = db)

    return dict(
        type = 'sql',
        queries_dir = Path(root_dir / 'queries' / name),
        schema_name = schema_name.format(**global_params),
        base_table_name = base_table_name.format(**global_params),
        db = db,
        md = md)


def mk_datasources(root_dir, get_password, config, global_params = dict()):
    return {
        k: mk_datasource(root_dir, get_password, k, global_params = global_params, **v)
        for k, v in config.items()}


@multimethod(key = 'type')
def sync_base(df_base, col_types, type, **kwargs):
    '''Replicates ``df_base`` to the given datasource.'''


@sync_base.register
def sync_base_sql(df_base, col_types, *, db, md, base_table_name):
    table_key = '.'.join([md.schema, base_table_name])

    tbl = (
       md.tables[table_key]
       if table_key in md.tables
       else Table(
         base_table_name,
         md,
         *map(lambda x: Column(x.name, x.type), col_types)))

    with db.begin() as tx:
        tbl.drop(tx, checkfirst = True)
        tbl.create(tx)

    sql_dump_from_pandas_chunked(db, df_base, tbl)


def sync_base_all(df_base, datasources):
    col_types = detect_column_types(df_base)
    for name, datasource in datasources.items():
        logging.info(f'syncing base table with `{name}`...')
        sync_base(df_base, col_types, **datasource)
        logging.info(f'done')



serialize_params = _c(''.join, _map(lambda s: '_' + s, '-'.join, second(str)), _sort(fst), L.items())


def fetch_extract(cfg, datasources, df_base, primary_key_cols, global_params):
    results = dict()

    dataset_params = cfg.get('parameters', dict())
    dataset_name = cfg['name'] + serialize_params(dataset_params)

    logging.info(f'fetching raw data for `{dataset_name}`')
    df = fetch_data(
        datasources[cfg['datasource']], cfg['name'],
        {**global_params, **dataset_params})

    logging.info(f'done')

    for e in cfg['extractors']:
        extractor_params = e.get('parameters', dict())
        extractor_name = e['name'] + serialize_params(extractor_params)

        logging.info(f'extracting `{extractor_name}`')
        results[(dataset_name, extractor_name)] = extract(
            e['name'],
            (df_base[_c(list, set)(primary_key_cols + e['merge_on'])]
             .merge(df, how = 'inner', on = e['merge_on'])),
            primary_key_cols,
            **{**global_params, **dataset_params, **extractor_params})

        logging.info(f'done')
    return results
