import operator as op
import numpy as np
import pandas as pd
import logging
from heapq import heappush, heappop

from ad_hoc_utils.common.f import multimethod, fst, _c, _l, _r, _map, _sort, ignore_unused, iterate, bypass
from ad_hoc_utils.common.f import L, P, bypass, identity, max_by, cached
from ad_hoc_utils.common.algo import simulated_annealing, mk_dp_solver
from ad_hoc_utils.common.imp import extras



xgb_hyperparams_space = dict(
    n_estimators     = (int,   (2,     100)),  # noqa: E221,E241
    max_depth        = (int,   (2,     15)),   # noqa: E221,E241
    gamma            = (float, (0.0,   1.0)),  # noqa: E221,E241
    eta              = (float, (0.001, 0.5)),  # noqa: E221,E241
    colsample_bytree = (float, (0.125, 1)),    # noqa: E221,E241
    subsample        = (float, (0.125, 1)),    # noqa: E221,E241
    reg_lambda       = (float, (0.0,   1.0)),  # noqa: E221,E241
    min_child_weight = (int,   (2,     20)),   # noqa: E221,E241
    max_leaves       = (int,   (2,     256)))  # noqa: E221,E241


lgbm_hyperparams_space = dict(
    n_estimators     = (int,   (2,     100)),  # noqa: E221,E241
    max_depth        = (int,   (2,     15)),   # noqa: E221,E241
    learning_rate    = (float, (0.001, 0.5)),  # noqa: E221,E241
    colsample_bytree = (float, (0.125, 1)),    # noqa: E221,E241
    subsample        = (float, (0.125, 1)),    # noqa: E221,E241
    reg_lambda       = (float, (0.0,   1.0)),  # noqa: E221,E241
    min_child_weight = (int,   (2,     20)),   # noqa: E221,E241
    num_leaves       = (int,   (2,     256)))  # noqa: E221,E241


cb_hyperparams_space = dict(
    n_estimators      = (int,   (2,     100)), # noqa: E221,E241
    max_depth         = (int,   (2,     15)),  # noqa: E221,E241
    learning_rate     = (float, (0.001, 0.5)), # noqa: E221,E241
    subsample         = (float, (0.125, 1)),   # noqa: E221,E241
    random_strength   = (float, (0.1,   0.9)), # noqa: E221,E241
    colsample_bylevel = (float, (0.125, 1)),   # noqa: E221,E241
    reg_lambda        = (float, (0.0,   1.0)), # noqa: E221,E241
    min_child_samples = (int,   (2,     30)))  # noqa: E221,E241


dtc_hyperparams_space = dict(
    max_depth         = (int,   (1,     16)),   # noqa: E221,E241
    min_samples_leaf  = (float, (1e-12, 0.5)),  # noqa: E221,E241
    min_samples_split = (float, (1e-12, 1.0)))  # noqa: E221,E241




def random_hps(rng, space):
    r = []
    for k, (t, (low, high)) in space.items():
        v = None
        if t == int:
            v = rng.integers(low, high + 1)
        elif t == float:
            v = rng.uniform(low, high)
        r.append((k, v))
    return dict(r)


@multimethod(key = 'method')
def search_hyperparams(f, space, *, n_iter, method, seed = None, **kwargs):
    '''
    Searches for an optimal (maximal) value of ``f`` over ``space``.
    '''


def random_mask(rng, n_columns, n_features):
    return np.isin(
        np.arange(n_columns),
        rng.choice(range(n_columns), size = n_features, replace = False))


@multimethod(key = 'method')
def search_features(eval_fs, n_columns, *, method, **kwargs):
    '''
    Searches for a subset of features with mimimal loss.
    Returns binary "mask" of remaining features.
    '''

def coerce_sample_types(space, p):
    p1 = {}
    for k, v in p.items():
        if k not in space:
            p1[k] = v
        else:
            v_type, (low, high) = space[k]
            v1 = v_type(v)
            if v1 < low:
                v1 = low
            if v1 > high:
                v1 = high
            p1[k] = v1
    return p1


@search_hyperparams.register
def search_hyperparams_random(f, space, *, n_iter, seed = None, verbose = False):
    rng = np.random.default_rng(seed)
    results = []

    for _ in range(n_iter):
        hps = random_hps(rng, space)
        val = f(hps)
        results.append((val, hps))
        if verbose:
            logging.info(f'val: {val}')

    result = _c(fst, list, reversed, _sort(fst))(results)
    if verbose:
        logging.info(f'done, best result: {result[0]}')
    return result[1]



with extras.modules('hyperopt'):
    from hyperopt import hp, fmin, tpe
    logging.getLogger('hyperopt').setLevel(logging.ERROR)


    def mk_hyperopt_space(space):
        mapping = {
            int: lambda k, low, high: hp.choice(k, range(low, high + 1)),
            float: lambda k, low, high: hp.uniform(k, low, high)}
        return dict([
            (k, mapping[t](k, low, high))
            for k, (t, (low, high)) in space.items()])


    @search_hyperparams.register
    def search_hyperparams_hyperopt(f, space, *, n_iter, seed = None, verbose = False):
        f0 = lambda hps: -f(coerce_sample_types(space, hps))
        return coerce_sample_types(
            space,
            fmin(
                f0,
                mk_hyperopt_space(space),
                algo = tpe.suggest,
                max_evals = n_iter,
                rstate = np.random.default_rng(seed),
                verbose = verbose))


with extras.modules('bayes_opt'):
    from bayes_opt import BayesianOptimization

    def mk_bayesian_space(space):
        mapping = {
            int: lambda low, high: (low, high + 1),
            float: lambda low, high: (low, high)}
        return dict([
            (k, mapping[t](low, high))
            for k, (t, (low, high)) in space.items()])


    @search_hyperparams.register
    def search_hyperparams_bayesian(
            f, space, *,
            n_iter, seed = None, verbose = False, init_pct = 0.1):
        n_init_iter = max(len(space), int(init_pct * n_iter))
        n_search_iter = n_iter - n_init_iter

        optimizer = BayesianOptimization(
            f = lambda **hps: f(coerce_sample_types(space, hps)),
            pbounds = mk_bayesian_space(space),
            random_state = seed,
            verbose = 2 if verbose else 0)

        optimizer.maximize(init_points = n_init_iter, n_iter = n_search_iter)

        return _c(
            _l(coerce_sample_types, space),
            L['params'], fst, list, _sort(op.neg, L['target']), P.res)(optimizer)


@search_hyperparams.register
def search_hyperparams_genetic(
        f, space, *,
        n_iter, seed = None, verbose = False,
        pool_size = 10, mutation_rate = 0.1, candidates = []):

    rng = np.random.default_rng(seed)

    candidates = candidates.copy()
    pool = []

    def add_item(hps, num):
        val = f(hps)
        heappush(pool, (val, num, hps))
        if verbose:
            logging.info(f'add: {val}')

    for _ in range(n_iter):
        if len(candidates) > 0:
            add_item(candidates.pop(), _)

        elif len(pool) < pool_size:
            add_item(random_hps(rng, space), _)

        else:
            samples = [
                *_c(_map(L[2]), rng.choice)(pool, size = 2, replace = False),
                random_hps(rng, space)]
            p = [*[(1 - mutation_rate) * 0.5] * 2, mutation_rate]

            add_item({
                k: rng.choice(_c(list, _map(L[k]))(samples), p = p)
                for k in space.keys()}, _)

        while (len(pool) > 0) and (len(pool) > pool_size):
            rem = heappop(pool)[0]
            if verbose:
                logging.info(f'rem: {rem}')

    for _ in range(len(pool) - 1):
        heappop(pool)
    result = pool[0]

    if verbose:
        logging.info(f'done, best result: {result[0]}')

    return result[2]


@search_hyperparams.register
def search_hyperparams_simulated_annealing(
        f, space, *,
        n_iter, seed = None, method = None,
        **kwargs):

    def t_evaluate(s):
        return -f(s)

    def t_mutate(s, rng, t):
        return {
            k: _c(type_, _l(min, r), _l(max, l))(s[k] + 0.5 * (-1 + 2*rng.uniform()) * (r - l) * t + l)
            for k, (type_, (l, r)) in space.items()}

    rng = np.random.default_rng(seed = seed)

    return simulated_annealing(
        random_hps(rng, space),
        t_evaluate, t_mutate,
        seed = seed, n_iter = n_iter, **kwargs)


@search_features.register
def search_features_random(
        eval_fs, n_columns, *,
        method, n_features, seed = None, n_iter, verbose = False):

    rng = np.random.default_rng(seed)

    def f(mask):
        result = eval_fs(mask)
        if verbose:
            logging.info(f'val: {result}')
        return result

    (mask, result) = max_by(
        f, [random_mask(rng, n_columns, n_features) for _ in range(n_iter)],
        return_metric = True)

    if verbose:
        logging.info(f'done, best result: {result}')

    return mask


@search_features.register
def search_features_simulated_annealing(
        eval_fs, n_columns, *,
        method, seed = None, n_iter, **kwargs):

    t_evaluate = _c(op.neg, eval_fs)

    def t_mutate(s, rng, t):
        n_flips = int(np.ceil(n_columns * t))
        mutation = np.isin(
            np.arange(n_columns),
            rng.choice(range(n_columns), size = n_flips, replace = False))
        result = (s ^ mutation)
        if result.sum() == 0:
            result = np.identity(n_columns)[rng.integers(n_columns)].astype(bool)
        return result

    rng = np.random.default_rng(seed)
    s0 = rng.integers(2, size = n_columns).astype(bool)

    return simulated_annealing(s0, t_evaluate, t_mutate, seed = seed, n_iter = n_iter, **kwargs)


@search_features.register
def search_features_dynamic(
        eval_fs, n_columns, *,
        method, seed = None, verbose = False, n_features, depth = 1,
        **kwargs):

    f = _c(bypass(lambda v: logging.info(f'get val: {v}') if verbose else identity),
           cached(lambda mask: tuple(mask))(eval_fs))

    d = mk_dp_solver(
        (lambda i, j: i == 0, lambda d, i, j: np.identity(n_columns)[j].astype(bool)),
        lambda d, i, j: max_by(f, [np.identity(n_columns)[j].astype(bool)] + [
            np.identity(n_columns)[j].astype(bool) | d[i1, j1]
            for i1 in range(max(0, i - depth), i)
            for j1 in range(n_columns)
            if (d[i1, j1] & np.identity(n_columns)[j].astype(bool)).sum() == 0
        ]))

    return max_by(f, [d[n_features - 1, j] for j in range(n_columns)])


remove_feature = lambda mask, i: mask & (~(np.identity(len(mask))[i].astype(bool)))


add_feature = lambda mask, i: mask | np.identity(len(mask))[i].astype(bool)


_with_logging = lambda f, verbose: bypass(lambda v: logging.info(f'val: {v}'))(f) if verbose else f


def _backward_selection_step(mask, eval_fs, pre_check, post_check):
    if pre_check(mask):

        new_mask, result = max_by(
            eval_fs,
            [remove_feature(mask, i) for i in np.nonzero(mask)[0]],
            return_metric = True)

        if (new_mask is not None) and post_check(result):
            return new_mask


@search_features.register
def search_features_backward_selection(
        eval_fs, n_columns, *,
        method = None, seed,
        max_q_lose = None, n_features = None, verbose = False):

    assert (max_q_lose is not None) or (n_features is not None), \
    'either of ``max_q_lose`` or ``n_features`` should be provided'

    init_mask = np.ones(shape = n_columns, dtype = bool)
    init_result = eval_fs(init_mask)

    f = _r(
        _backward_selection_step,
        eval_fs = _with_logging(eval_fs, verbose),
        pre_check = lambda mask: mask.sum() > 1 + (n_features or 0),
        post_check = lambda result: (max_q_lose is None) or ((result - init_result)/abs(init_result) >= -max_q_lose/100.0))

    return list(iterate(f, init_mask))[-1]


@search_features.register
def search_features_forward_selection_by_n_features(
        eval_fs, n_columns, *,
        method = None, n_features, seed, verbose = False):

    rng = np.random.default_rng(seed)

    mask = None
    result = None

    best_mask = np.zeros(shape = n_columns, dtype = bool)

    while best_mask.sum() < n_features:
        best_result = None
        for i in np.nonzero(~best_mask)[0]:
            mask = add_feature(best_mask, i)

            result = eval_fs(mask)
            if (best_result is None) or (result > best_result):
                best_result = result
                idx_include = i

        best_mask = add_feature(best_mask, idx_include)

    return best_mask


@search_features.register
def search_features_forward_selection_by_quality(
        eval_fs, n_columns, *,
        method = None, min_q_improve, seed, verbose = False):

    rng = np.random.default_rng(seed)

    improve = None

    mask = None
    result = None

    best_mask = search_features_forward_selection_by_n_features(eval_fs, n_columns, n_features = 1, seed = seed)
    init_result = eval_fs(best_mask)

    while True:
        best_result_rel = None
        for i in np.nonzero(~best_mask)[0]:
            mask = add_feature(best_mask, i)

            result = eval_fs(mask)
            improve = (result - init_result) / abs(init_result)
            if improve >= min_q_improve:
                if (best_result_rel is None) or (improve > best_result_rel):
                    best_result_rel = improve
                    idx_include = i

        if best_result_rel is None:
            break
        else:
            init_result = best_result_rel * abs(init_result) + init_resuult
            best_mask = add_feature(best_mask, idx_include)

    return best_mask



def block_cv_split(dts, train_depth, test_depth, n_blocks):
    min_dt = dts.min() + pd.Timedelta(days = train_depth)
    max_dt = dts.max() - pd.Timedelta(days = test_depth)

    assert min_dt < max_dt, 'expecting positive time delta'

    d = (max_dt - min_dt)/(n_blocks - 1)

    for i in range(n_blocks):
        snapshot_dt = min_dt + d * i
        idx_train = (dts > snapshot_dt - pd.Timedelta(days = train_depth)) & (dts < snapshot_dt)
        idx_test = (dts >= snapshot_dt) & (dts <= snapshot_dt + pd.Timedelta(days = test_depth))
        if idx_train.sum() == 0 or idx_test.sum() == 0:
            continue
        yield (idx_train, idx_test)


def nested_cv_split(dts, train_depth, test_depth, n_blocks):
    min_dt = dts.min() + pd.Timedelta(days = train_depth)
    max_dt = dts.max() - pd.Timedelta(days = test_depth)

    assert min_dt < max_dt, 'expecting positive time delta'

    d = (max_dt - min_dt)/(n_blocks - 1)

    for i in range(n_blocks):
        snapshot_dt = min_dt + d * i
        idx_train = (dts < snapshot_dt)
        idx_test = (dts >= snapshot_dt) & (dts <= snapshot_dt + pd.Timedelta(days = test_depth))
        if idx_train.sum() == 0 or idx_test.sum() == 0:
            continue
        yield (idx_train, idx_test)


__all__ = [
    'xgb_hyperparams_space',
    'lgbm_hyperparams_space',
    'cb_hyperparams_space',
    'dtc_hyperparams_space',

    'search_hyperparams',
    'search_features',

    'block_cv_split',
    'nested_cv_split']
