import pandas as pd
import numpy as np
from itertools import product
from ad_hoc_utils.common.f import *


def fix_cumulative_agg(xs, xg, agg, rn):
    match agg:
        case 'min': return xg.cummin().rename(xs.name)
        case 'max': return xg.cummax().rename(xs.name)
        case 'sum': return xg.cumsum().rename(xs.name)
        case 'avg': return (xg.cumsum()/rn).rename(xs.name)
    return xs


def fix_cumulative_aggs(df, primary_key_cols, rn_col):
    feature_cols = list(set(df.columns) - set([*primary_key_cols, rn_col]))

    dfg = df.groupby(primary_key_cols)
    return pd.concat([
        df[[*primary_key_cols, rn_col]],
        *[fix_cumulative_agg(df[c], dfg[c], c.split('_')[0], df[rn_col])
          for c in feature_cols]], axis = 1)


def make_filters(df, value_col, filter_col, vs):
    return pd.concat([
        pd.Series(np.where((df[filter_col] == v), df[value_col], np.nan), index = df.index).rename(f'{value_col}_{v}')
          for v in vs], axis = 1)


def add_suffix(df, primary_key_cols, suffix):
     return df.rename(lambda c: c if c in primary_key_cols else '_'.join([c, suffix]), axis = 1)


mk_abs_col_name = lambda a, v, i: f'{a}{i}_{v}'
mk_rel_col_name = lambda r, v, p: f'{r[0]}{p[0]}_to_{r[1]}{p[1]}_{v}'


def abs_features(df, aggs, vs, depth, group_cols, rn_col):
    abs_cs = list(product(aggs, vs, range(1, 1 + depth)))

    df = pd.concat([df, *[
        ((df['rn'] == i)*df[f'{a}_{v}']).rename(mk_abs_col_name(a, v, i))
        for a, v, i in abs_cs]], axis = 1)

    dfg = df.groupby(group_cols)
    return dfg[[mk_abs_col_name(a, v, i) for a, v, i in abs_cs]].sum().reset_index()


def rel_features(df, ratios, vs, depth):
    rel_cs = [
        (r, v, (i, j))
        for r in ratios
        for v in vs
        for i in range(1, 1 + depth)
        for j in range(1, 1 + depth)
        if (i < j) or ((i == j) and (r[0] != r[1]))]

    return pd.concat([df, *[
        (df[mk_abs_col_name(r[0], v, p[0])]
         / df[mk_abs_col_name(r[1], v, p[1])]).rename(
            mk_rel_col_name(r, v, p))
        for r, v, p in rel_cs]], axis = 1)




@multimethod(key = 'name', force = True)
def extract(name, df, primary_key_cols, /, **kwargs):
    '''Extracts features from raw data in ``df``'''





@multimethod(key = 'type')
def fit(type, params, *args, **kwargs):
    '''Fits transformer. '''


@multimethod(key = 'type')
def transform(type, params, state, *args, **kwargs):
    '''Applies transformer. '''

# contrib

@fit.register
def fit_impute(type, params, xs):
    return dict(value = xs.agg(params['method']))


@transform.register
def transform_impute(type, params, state, xs):
    return [xs.fillna(state['value']).rename('_'.join([xs.name, 'or', params['method']]))]


@fit.register
def fit_bound(type, params, xs):
    return _c(
        dict, _l(zip, ['left', 'right']),
        L.quantile([params['alpha']/2, 1 - params['alpha']/2]))(
        xs[np.isfinite(xs) & np.logical_not(np.isnan(xs))])


@transform.register
def transform_bound(type, params, state, xs):
    return [_c(
        L.rename('_'.join([xs.name, 'bound'])),
        _r(pd.Series, index = xs.index),
        _l(np.where, xs < state['left'], state['left']),
        _l(np.where, xs > state['right'], state['right']))(xs)]


@fit.register
def fit_null_encode(type, params, xs):
    return dict()


@transform.register
def transform_null_encode(type, params, state, xs):
    return [xs.isna().rename('_'.join([xs.name, 'is', 'null']))]


@fit.register
def fit_one_hot_encode(type, params, xs):
    return dict(values = _c(
        list, _map(fst),
        _filter(L > params['significance_threshold'], snd),
        L.items())(xs.value_counts()/(~xs.isna()).sum()))

@transform.register
def transform_one_hot_encode(type, params, state, xs):
    return [
        (xs == v)
        .astype(int)
        .rename('_'.join([xs.name, 'cat', str(i)]))
        for i, v in enumerate(state['values'])]



@fit.register
def fit_tsfresh_impute(type, params, xs):
    xs = xs[(~np.isnan(xs)) & np.isfinite(xs)]
    return (
        dict(min = 0, max = 0, median = 0)
        if len(xs) == 0
        else dict(min = xs.min(), max = xs.max(), median = xs.median()))


@transform.register
def transform_tsfresh_impute(type, params, state, xs):
     return [_c(
         L.rename(xs.name),
         _r(pd.Series, index = xs.index),
         lambda xs: np.where(np.isnan(xs), state['median'], xs),
         lambda xs: np.where(xs == np.inf, state['max'], xs),
         lambda xs: np.where(xs == -np.inf, state['min'], xs))(xs)]


def mk_transformers(rules, df):
    transformers = []

    for (c, rs) in rules:
        ts = []
        xs = df[c]
        for r_type, r_params in rs:
            r_state = fit(r_type, r_params, xs)
            xs = transform(r_type, r_params, r_state, xs)[0] # TODO: this is a workaround
            ts.append((r_type, r_params, r_state))
        transformers.append((c, ts))

    return transformers


def apply_transformers(xs, ts):
    xss = [xs]
    for args in ts:
        xss = transform(*args, xss[0]) # TODO: this is a workaround
    return xss
