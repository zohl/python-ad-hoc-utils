from functools import reduce, wraps

import numpy as np
import pandas as pd
import operator as op

from ad_hoc_utils.common.f import multimethod, _c, _l, _r, _map, L, _map, _sort, spread, lift_maybe, min_by
from ad_hoc_utils.common.algo import mk_dp_solver
from ad_hoc_utils.common.imp import extras

@multimethod(key = 'method')
def mk_bins(xs, *args, method, **kwargs):
    """
Splits ``xs`` according to specified ``method``.


Parameters
----------
xs: array_like
    vector to split
ys: array_like
    vector of events (optional, depending on the method)
method: string
    method to use:

    - ``'equiwidth'``
    - ``'equisized'``
    - ``'quantile'``
    - ``'cart'``
    - ``'woe'``
    - ``'dynamic'``

    For differences and use cases see the tables below.

Returns
-------
list of ``dict``
    A list of bins. Each bin has the following structure

    - ``l`` -- left bound (inclusive)
    - ``r`` -- right bound (inclusive)
    - ``n_total`` -- the number of items in the bin
    - ``n_events`` -- the number of events in the bin (if ``ys`` is provided)



Examples
--------
>>> mk_bins([1,2,3,4], n_bins = 2, method = 'quantile')
[{'l': 1, 'r': 2, 'n_total': 2}, {'l': 3, 'r': 4, 'n_total': 2}]


>>> n_samples = 100
>>> rng = np.random.default_rng(seed = 1234)
>>> xs = rng.normal(size = n_samples)
>>> ys = rng.integers(2, size = n_samples)
>>> mk_bins(xs, ys, method = 'cart', max_depth = 3, min_samples_split = 0.3)
[{'l': -2.491789618298251,
  'r': -1.1741498690098522,
  'n_total': 17,
  'n_events': 8},
 {'l': -1.1574108072969482,
  'r': -0.2996388895736321,
  'n_total': 18,
  'n_events': 15},
 {'l': -0.2472150147600114,
  'r': -0.16380577789126796,
  'n_total': 3,
  'n_events': 0},
 {'l': -0.1490677784218646,
  'r': 0.5130902145495575,
  'n_total': 22,
  'n_events': 13},
 {'l': 0.5194931990183601,
  'r': 2.913099222503971,
  'n_total': 40,
  'n_events': 17}]


>>> n_samples = 100
>>> rng = np.random.default_rng(seed = 1234)
>>> xs = rng.normal(size = n_samples)
>>> ys = (xs > 0).astype(int)
>>> mk_bins(xs, ys, method = 'woe', min_size = 5, p_threshold = 0.05)
[{'l': 0.013181579118739723,
  'r': 2.913099222503971,
  'n_total': 56,
  'n_events': 56},
 {'l': -2.491789618298251,
  'r': -0.010978255182129314,
  'n_total': 44,
  'n_events': 0}]


Notes
-----

The bounds of the bins are inclusive. To split another vector ``xs1`` by
resulting bins ``bs`` use the right bound:

>>> np.searchsorted(_c(list, _map(L['r']))(bs), xs1)

The list of dictionaries might be hard to read. It's possible to represent the
bins ``bs`` as a table using ``pandas``:

>>> display(pd.DataFrame(bs))

"""


def bin_separate_nan(f):
    @wraps(f)
    def result(xs, ys = None, *args, **kwargs):
        result = []

        if len(xs) == 0:
            return result

        xs = np.array(xs)
        ys = lift_maybe(np.array)(ys)

        idx_nan = np.isnan(xs)

        if idx_nan.sum() > 0:
            nan_bin = dict(l = np.nan, r = np.nan, n_total = idx_nan.sum())
            if ys is not None:
                nan_bin['n_events'] = ys[idx_nan].sum()
            result.append(nan_bin)

            xs = xs[~idx_nan]
            ys = lift_maybe(L[~idx_nan])(ys)

        return (result + f(xs, ys, *args, **kwargs))

    return result


def bin_group(f):
    @wraps(f)
    def result(xs, ys = None, *args, **kwargs):
        vs = pd.Series(xs).value_counts().sort_index()

        # TODO: pass grouped ys here
        splits = f(list(vs), None, *args, **kwargs)

        return np.searchsorted(
            np.array(splits),
            np.searchsorted(np.array(vs.index), xs),
            side = 'right')

    return result


def bin_split(f):
    @wraps(f)
    def result(xs, ys = None, *args, **kwargs):
        bs = f(xs, ys, *args, **kwargs)

        dfg = pd.DataFrame(dict(
            b = bs,
            x = xs,
            y = (ys if ys is not None else [0]*len(xs)))).groupby('b')

        keys = ['l', 'r', 'n_total']
        if ys is not None:
            keys.append('n_events')

        return _c(list, _map(dict, _l(zip, keys)), zip)(
            dfg['x'].min(),
            dfg['x'].max(),
            dfg.size(),
            dfg['y'].sum())

    return result


@mk_bins.register
@bin_split
def mk_bins_singleton(xs, ys = None, *, method):
    return range(len(xs))


@mk_bins.register
@bin_separate_nan
@bin_split
def mk_bins_equiwidth(xs, ys = None, *, method, n_bins):
    """
    Guarantees:
    - bin's width is limited and doesn't exceed ``(max(xs) - min(xs))/n_bins``
    - bins do not have overlapping bounds

    Does not guarantee:
    - bins' sizes have low variance
    """

    splits = np.linspace(np.min(xs), np.max(xs), 1 + n_bins)[1:-1]
    return np.searchsorted(splits, xs)


@mk_bins.register
@bin_separate_nan
@bin_split
def mk_bins_equisized(xs, ys = None, *, method, n_bins):
    """
    Guarantees:
    - bins' sizes have low variance

    Does not guarantee:
    - bin's width is limited and doesn't exceed ``(max(xs) - min(xs))/n_bins``
    - bins do not have overlapping bounds
    """

    splits = np.linspace(0, len(xs), 1 + n_bins)[1:-1]
    df = (pd.DataFrame(dict(x = xs))
          .reset_index(drop = False)
          .sort_values('x')
          .reset_index(drop = True))
    df['b'] = np.searchsorted(splits, df.index, side = 'right')
    return df.sort_values('index')['b'].to_numpy()


@mk_bins.register
@bin_separate_nan
@bin_split
def mk_bins_quantile(xs, ys = None, *, method, n_bins):
    """
    Guarantees:
    - bins do not have overlapping bounds

    Does not guarantee:
    - bin's width is limited and doesn't exceed ``(max(xs) - min(xs))/n_bins``
    - bins' sizes have low variance
    - number of bins as specified in ``n_bins``
    """

    splits = np.quantile(xs, np.linspace(0, 1, 1 + n_bins)[1:-1])
    return np.searchsorted(splits, xs, side = 'right')


@multimethod(key = 'method')
def merge_bins(bins, *, method, **kwargs):
    pass

@merge_bins.register
def merge_bins_concat(bins, *, method):
    return [] if len(bins) == 0 else [{
        k: reduce(f, map(L[k], bins))
        for k, f in dict(
            l = min,
            r = max,
            n_total = op.add,
            n_events = op.add).items()}]


@merge_bins.register
def merge_bins_event_rate(bins, *, method):
    has_changed = True
    new_bins = bins

    while has_changed:
        has_changed = False

        old_bins = new_bins
        new_bins = []

        i = 0
        while i < len(old_bins):
            b = dict(**old_bins[i])
            i += 1
            while i < len(old_bins):
                b1 = old_bins[i]
                if b['n_events']/b['n_total'] <= b1['n_events']/b1['n_total']:
                    [b] = merge_bins([b, b1], method = 'concat')
                    i += 1
                else:
                    break
            new_bins.append(b)
        has_changed = (len(old_bins) > len(new_bins))

    return new_bins


@mk_bins.register
@bin_separate_nan
@bin_split
@bin_group
def mk_bins_dynamic(xs, ys, *, method, n_bins):
    if n_bins > len(xs):
        n_bins = len(xs)

    target_mean = sum(xs)/n_bins

    d = mk_dp_solver(
        ((0, 0), dict(p = 0, e = 0)),
        lambda d, i, j: _c(
            _l(min_by, L['e']),
            _map(lambda p: dict(
                    p = p,
                    e = d[i - 1, p]['e'] + (sum(xs[p:j]) - target_mean)**2)))(
            {0} if i == 1 else range(i - 1, j)))

    ps = []
    j = len(xs)
    for i in reversed(range(1, n_bins)):
        s = d[i + 1, j]
        ps.append(s['p'])
        j = s['p']

    return  ps[::-1]



with extras.modules('sklearn'):
    from sklearn.tree import DecisionTreeClassifier

    @mk_bins.register
    @bin_separate_nan
    @bin_split
    def mk_bins_cart(xs, ys, *, method, **dtc_kwargs):

        dtc_default_kwargs = dict(
            max_depth = 2,
            min_samples_split = 0.1,
            min_samples_leaf = 0.01)

        dtc = DecisionTreeClassifier(**{**dtc_default_kwargs, **dtc_kwargs})
        dtc.fit(np.array(xs).reshape(-1, 1), ys)
        idx_leaves = (dtc.tree_.children_left == dtc.tree_.children_right)
        splits = sorted(set(dtc.tree_.threshold[~idx_leaves]))
        return np.searchsorted(splits, xs, side = 'right')


with extras.modules('scipy'):
    import scipy.stats as st

    def bins_proximity(b0, b1):
        '''
        Measures the significance of the difference between the given bins.

        More precisely, each bin represents a sample of Bernoulli distibution. We perform two-sided z-test and return p-value of it.
        '''

        n0 = b0['n_total']
        n1 = b1['n_total']

        m0 = b0['n_events']
        m1 = b1['n_events']

        p0 = m0/n0
        p1 = m1/n1
        z = (p1 - p0)/np.sqrt(1e-16 + p0*(1 - p0)/n0 + p1*(1 - p1)/n1)

        return 2*(1 - st.norm.cdf(abs(z)))


    @merge_bins.register
    def merge_bins_proximity(bins, *, method, min_size, p_threshold):
        has_changed = True
        new_bins = bins

        while has_changed:
            has_changed = False

            old_bins = new_bins
            new_bins = []

            ps = _c(
                list,
                _map(spread(
                    lambda b0, b1: (
                        1.0
                        if (b0['n_total'] < min_size or b1['n_total'] < min_size)
                        else bins_proximity(b0, b1)))),
                zip)(old_bins, old_bins[1:])


            if len(ps) > 0:
                idx_max = np.argmax(ps)

                if ps[idx_max] > p_threshold:
                    has_changed = True
                    new_bins =  [
                        *old_bins[:idx_max],
                        *merge_bins(
                            [old_bins[idx_max], old_bins[idx_max + 1]],
                            method = 'concat'),
                        *old_bins[idx_max + 2:]]

            if not has_changed:
                new_bins = old_bins

        return new_bins



    @mk_bins.register
    @bin_separate_nan
    def mk_bins_woe(xs, ys, *, method, min_size, p_threshold):
        return _c(
            _r(merge_bins, method = 'proximity', min_size = min_size, p_threshold = p_threshold),
            _r(merge_bins, method = 'event_rate'),
            _sort(op.neg, L['l']),
            _r(mk_bins, method = 'singleton'))(xs, ys)


__all__ = [
    'mk_bins']
