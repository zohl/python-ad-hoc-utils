import yaml
from pathlib import Path


def from_yaml(
        path: str | Path, *,
        loader: Type[yaml.BaseLoader] = yaml.Loader,
        schema_path: str | Path | None = None): ...
