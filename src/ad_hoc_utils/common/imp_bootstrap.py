import logging
import sys
from importlib import import_module
from pathlib import Path
import packaging.version as version


COMPAT_PREFIX = '_compat-'

def get_compats(__file__):

    sys_version = version.parse('.'.join(map(str, [
        sys.version_info.major,
        sys.version_info.minor,
        sys.version_info.micro])))

    return list(map(
        lambda x: x[1],
        sorted(
            filter(
                lambda x: x[0] <= sys_version,
                map(
                    lambda x: (version.parse(x[len(COMPAT_PREFIX):].replace('-', '.')), x),
                    [f.stem for f in Path(__file__).parent.glob(COMPAT_PREFIX + '*.py')])))))

# TODO:
# - spec optional
# - spec reexport

def import_all_from_submodule(g, __name__, submodule_name):
    m = import_module('.' + submodule_name, __name__)
    if m is None:
        return

    if not hasattr(m, '__all__'):
        logging.warning(f'module {__name__}{submodule_name} does not declare ``__all__``')
        return

    for k in m.__all__:
        g[k] = m.__dict__[k]

    g['__all__'] = list(set(g.get('__all__', []) + m.__all__))


def import_all_from_submodules(g, __name__, *submodule_names, **kwargs):
    for submodule_name in submodule_names:
        import_all_from_submodule(g, __name__, submodule_name, **kwargs)
