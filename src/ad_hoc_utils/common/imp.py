from importlib import import_module
from importlib.util import find_spec
import logging
from configparser import ConfigParser
from pathlib import Path
import sys
import re

from itertools import chain
import operator as op
from ad_hoc_utils.common.f import _c, _map, _filter, _sort, L, multimethod
from ad_hoc_utils.common.f import cached, second


CONFIG_DIR = '.config'
SETUP_FILE = 'setup.cfg'
REQUIRE_SECTION = 'options.extras_require'
MAPPING_SECTION = 'options.extras_mapping'


parse_config_list = _c(list, _filter(L > 0, len), L.split('\n'))


def parse_config_deps(data):
    """
    Parses contents of ``'options.extras_require'`` section and resolves nested dependencies.


    """
    deps = dict()

    for k, vs in data.items():
        result = []
        for v in parse_config_list(vs):
            if v.startswith('@'):
                assert v[1:] in deps, f'missing reference {v}'
                result += deps[v[1:]]
            else:
                result.append(v)
        deps[k] = _c(_sort(), set)(result)

    return deps


@multimethod(key = lambda name: name.replace('.', '_'))
def update_setup(name, cfg, data, **kwargs):
    '''Alters ``setup.cfg`` sections.'''

@update_setup.register
def update_setup_options_extras_require(name, cfg, data):
    for k, vs in parse_config_deps(data).items():
        cfg[REQUIRE_SECTION][k] = '\n' + '\n'.join(vs)


@update_setup.register
def update_setup_options_extras(name, cfg, data):
    if ('add_all' in data) and data['add_all']:
        cfg[REQUIRE_SECTION]['all'] = '\n' + _c('\n'.join, _sort(), set)(
            parse_config_list(''.join([
                v for k, v in cfg[REQUIRE_SECTION].items() if k != 'all'])))


def update_setup_config(root_dir, verbose = False, **kwargs):
    """
    Updates ``setup.cfg`` based on additional configuration files.


    Warning: this fuction supports only "src"-layout.
    """
    root_dir = Path(root_dir)

    fn_cfg_base = root_dir / SETUP_FILE
    if not fn_cfg_base.exists():
        logging.error('specified dir does not contain `{SETUP_FILE}` file')

    cfg_base = ConfigParser()
    _c(cfg_base.read_string, L.read_text())(fn_cfg_base)

    for fn_cfg in _c(
            _filter(L.exists()),
            _map(L / SETUP_FILE, L / CONFIG_DIR),
            _filter(op.not_, L.endswith('.egg-info'), str),
            L.iterdir())(root_dir / 'src'):

        cfg = ConfigParser()
        _c(cfg.read_string, L.read_text())(fn_cfg)

        for k, v in cfg.items():
            if k.replace('.', '_') in update_setup._handlers:
                if verbose:
                    logging.info(f'applying `{k}`')
                update_setup(k, cfg_base, v)

    _c(cfg_base.write, L.open('w'))(Path(fn_cfg_base))


root_package = lambda name: name.split('.')[0]


parent_package = lambda name: '.'.join(name.split('.')[:-1])


def is_module_available(m):
    return ((m in sys.modules) or (find_spec(m) is not None))


class ExtrasImportError(Exception): pass


class ExtrasContext:
    def __init__(self, *modules):
        self._modules = modules
        self._is_available = all(map(is_module_available, self._modules))


    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        if (exc_type is ModuleNotFoundError) and exc_value.name in self._modules:
            return True

    @property
    def is_available(self):
        return self._is_available


def strip_version(s):
    return re.match('^[^<>= @]+', s)[0]


class extras:
    """
    Provides an interface to manage optional dependencies.


    """
    @classmethod
    @cached()
    def _get_package_deps(cls, __name__):

        module = sys.modules[root_package(__name__)]
        if not hasattr(module, '__path__'):
            return dict()

        result = []

        for path in module.__path__:

            setup_cfg = Path(path) / CONFIG_DIR / SETUP_FILE
            if not setup_cfg.exists():
                continue

            cfg = ConfigParser()
            _c(cfg.read_string, L.read_text())(setup_cfg)

            if REQUIRE_SECTION not in cfg:
                continue

            deps = {
                k: _c(list, _map(strip_version))(v)
                for k, v in parse_config_deps(cfg[REQUIRE_SECTION]).items()}

            if MAPPING_SECTION in cfg:
                mapping = _c(
                    dict,
                    _map(second(parse_config_list)))(
                            cfg[MAPPING_SECTION].items())
                deps = _c(
                    dict,
                    _map(second(
                        _c(list, chain.from_iterable,
                           _map(lambda v: mapping.get(v, [v]))))))(deps.items())

            result.append(deps)

        return _c(dict, chain.from_iterable, _map(L.items()))(result)


    @classmethod
    def _get_modules(cls, __name__, deps):
        r = cls._get_package_deps(__name__)
        return _c(
            set, chain.from_iterable,
            _map(cls._get_package_deps(__name__).__getitem__))(deps)


    @classmethod
    def modules(cls, *ms):
        return ExtrasContext(*ms)


    @classmethod
    def deps(cls, __name__, *deps):
        return cls.modules(*cls._get_modules(__name__, deps))


    @classmethod
    def require_modules(cls, *ms):
        missing_modules = _c(list, _filter(op.not_, is_module_available))(ms)
        if len(missing_modules) > 0:
            raise ExtrasImportError(missing_modules)


    @classmethod
    def require_deps(cls, __name__, *deps):
        return cls.require_modules(*cls._get_modules(__name__, deps))


def def_if_not_exists(g, **kwargs):
    """Adds to namespace ``g`` variables from ``kwargs``, but only if they aren't presented there already.

    Example:

    >>> foo = 24
    >>> def_if_not_exists(globals(), dict(foo = 42, bar = 13))
    >>> (foo, bar)
    (24, 13)

    """
    for k, v in kwargs.items():
        if k not in g:
            g[k] = v


def import_optional_module(name, package):
    try:
        return import_module(name, package)
    except ExtrasImportError as e:
        logging.info('optional dependencies for {} are not installed: {}'.format(
            name if package is None else f'{package}{name}',
            ', '.join(e.args[0])))


def import_all_from_submodules(g, __name__, *submodules):
    for submodule_name in submodules:
        m = import_optional_module(submodule_name, __name__)

        if m is None:
            continue

        if not hasattr(m, '__all__'):
            logging.warning(f'module {__name__}{submodule_name} does not declare ``__all__``')
            continue

        def_if_not_exists(g, **{k: m.__dict__[k] for k in m.__all__})


def import_tests_from_submodules(g, __name__, *submodules):
    for submodule_name in submodules:
        m = import_optional_module(submodule_name, parent_package(__name__))

        if m is None:
            continue

        def_if_not_exists(g, **{k: v for k, v in m.__dict__.items() if k.startswith('test_')})


if __name__ == '__main__':
    from argparse import ArgumentParser

    logging.basicConfig(
        format = '%(asctime)s %(levelname)s %(message)s',
        datefmt = '%Y.%m.%d %H:%M:%S',
        level = logging.INFO)

    parser = ArgumentParser()
    subparser = parser.add_subparsers(dest = 'which')

    p = subparser.add_parser('update_setup_config')
    p.add_argument('root_dir')

    args = parser.parse_args()

    if args.which == 'update_setup_config':
        update_setup_config(args.root_dir, verbose = True)

    else:
        parser.print_help()
