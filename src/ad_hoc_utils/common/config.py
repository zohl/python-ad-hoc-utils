from abc import ABCMeta, abstractmethod
from typing import Type
from getpass import getpass
import base64
from pathlib import Path
import os
import yaml
from jsonschema import Draft202012Validator
from ad_hoc_utils.common.f import _c, _map


class BaseFetcher(metaclass = ABCMeta):
    @abstractmethod
    def __call__(self, *args, **kwargs):
        pass

    def on_cache(self, value, *args, **kwargs):
        pass


class UserInputFetcher(BaseFetcher):
    def __init__(self, fmt):
        self._fmt = fmt

    def __call__(self, *args, **kwargs):
        return getpass(prompt = self._fmt.format(*args, **kwargs) + ':')

from_user_input = UserInputFetcher

class B64FileFetcher(BaseFetcher):
    def __init__(self, fmt, cache = False):
        self._fmt = fmt
        self._cache = cache

    def _mk_key(self, *args, **kwargs):
        return Path(self._fmt.format(*args, **kwargs))

    def __call__(self, *args, **kwargs):
        key = self._mk_key(*args, **kwargs)
        if key.exists():
            return base64.b64decode(key.read_bytes()).decode('utf-8')

    def on_cache(self, value, *args, **kwargs):
        if self._cache:
            self._mk_key(*args, **kwargs).write_bytes(base64.b64encode(value.encode('utf-8')))

from_b64file = B64FileFetcher


class EnvironmentFetcher(BaseFetcher):
    def __init__(self, fmt, cache = False):
        self._fmt = fmt
        self._cache = cache

    def _mk_key(self, *args, **kwargs):
        return (self._fmt.format(*args, **kwargs)
            .upper()
            .replace('.', '_')
            .replace('-', '_'))

    def __call__(self, *args, **kwargs):
        return os.environ.get(self._mk_key(*args, **kwargs))

    def on_cache(self, value, *args, **kwargs):
        if self._cache:
            os.environ[self._mk_key(*args, **kwargs)] = str(value)

from_env = EnvironmentFetcher


class JoinFetcher(BaseFetcher):
    def __init__(self, lhs, rhs):
        self._lhs = lhs
        self._rhs = rhs

    def __call__(self, *args, **kwargs):
        value = self._lhs(*args, **kwargs)
        if value is None:
            value = self._rhs(*args, **kwargs)
            if value is not None:
                self._lhs.on_cache(value, *args, **kwargs)
        return value

    def on_cache(self, value, *args, **kwargs):
        self._lhs.on_cache(value, *args, **kwargs)
        self._rhs.on_cache(value, *args, **kwargs)

setattr(BaseFetcher, '__or__', lambda self, other: JoinFetcher(self, other))



class MetaYAMLCustomLoader(type):
    def __new__(
            cls, name, bases, namespace,
            base = yaml.Loader,
            implicit_resolvers_blacklist = [],
            constructors = dict()):

        implicit_resolvers = {
            key: [(tag, regex)
                  for tag, regex in mappings
                  if tag not in implicit_resolvers_blacklist]
            for key, mappings in base.yaml_implicit_resolvers.items()}

        result = super().__new__(
            cls, name, (base, *bases),
            {**namespace, **dict(yaml_implicit_resolvers = implicit_resolvers)})

        for k, v in constructors.items():
            result.add_constructor('!' + k, v)

        return result


class YAMLValidationException(Exception):
    def __init__(self, validator, data):
        super().__init__(
                'Failed to pass the validation',
                validator.schema['$id'],
                _c(list,
                   _map(lambda r: r.json_path + ': ' + r.message),
                   validator.iter_errors)(data))

def from_yaml(path, *, loader = yaml.Loader, schema_path = None):
    result = yaml.load(Path(path).read_text(), loader)

    if schema_path is not None:
        schema = yaml.load(Path(schema_path).read_text(), loader)
        v = Draft202012Validator(schema)
        if not v.is_valid(result):
            raise YAMLValidationException(v, result)

    return result

__all__ = [
    'from_user_input',
    'from_b64file',
    'from_env',
    'MetaYAMLCustomLoader', 'YAMLValidationException', 'from_yaml']
