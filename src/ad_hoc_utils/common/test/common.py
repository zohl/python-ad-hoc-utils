import pytest
from pyexpect import expect
from hypothesis import given
import hypothesis.strategies as st
import operator as op
from functools import wraps


st_seed = lambda: st.integers(min_value = 0, max_value = (1 << 32) - 1)

st_column_names = lambda: st.text(alphabet = string.ascii_lowercase + '_', min_size = 4, max_size = 12).filter(lambda s: not s.startswith('_'))

with_column_names = lambda data: st.tuples(
    st.just(data),
    st.lists(st_column_names(), min_size = data[0].shape[1], max_size = data[0].shape[1], unique = True))

st_n_samples = lambda: st.integers(min_value = 500, max_value = 2500)
st_n_features = lambda: st.integers(min_value = 50, max_value = 100)


class ExpectWrapper():
    """
    "Inverted" version of ``expect``.

    Synopsis:

    >>> expect(42).to_be.less_than(100)

    >>> f = expect_.to_be.less_than(100)
    >>> f(42)

    """

    def __init__(self, queue = []):
        self._queue = queue

    def __getattr__(self, k):
        return ExpectWrapper(self._queue + [k])

    def __call__(self, v):
        def result(actual):
            x = expect(actual)
            for k in self._queue:
                x = getattr(x, k)
            x(v)
        return result


expect_ = ExpectWrapper()


def as_fixture(ns):
    def decorator(f):

        @pytest.fixture(name = f.__name__)
        @wraps(f)
        def result(*args, **kwargs):
            return f(*args, **kwargs)

        ns[f.__name__ + '_fixture'] = result
        return f

    return decorator


__all__ = [
    'st_seed',
    'st_column_names', 'with_column_names',
    'st_n_samples', 'st_n_features',
    'expect_',
    'as_fixture']
