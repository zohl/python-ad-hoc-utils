import pytest
import time
import logging


@pytest.fixture
def seed():
    s = int(time.time() * ((1 << 16) + 1)) % ((1 << 16) + 1)
    logging.info(f'using seed {s}')
    return s
