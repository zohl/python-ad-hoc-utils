from ad_hoc_utils.common.imp import extras

extras.require_deps(__name__, 'ml-dev')

import pytest
from pyexpect import expect
from hypothesis import given
import hypothesis.strategies as st
import operator as op

import numpy as np
import pandas as pd
import hypothesis.extra.pandas as st_pd
from sklearn.datasets import make_classification, make_regression

import logging
from .common import *


@st.composite
def st_binary_problem(
        draw,
        st_n_samples = st_n_samples(),
        st_n_features = st_n_features(),
        st_event_rate = st.floats(min_value = 0.1, max_value = 0.9),
        seed = None,
        named = False):

    n_clusters_per_class = draw(st.integers(min_value = 2, max_value = 5))
    min_n_informative = 1 + int(np.ceil(np.log(n_clusters_per_class)/np.log(2)))

    n_samples = draw(st_n_samples)

    n_features = max(min_n_informative, draw(st_n_features))
    n_informative = draw(st.integers(min_value = min_n_informative, max_value = n_features))
    n_redundant = draw(st.integers(min_value = 0, max_value = n_features - n_informative))

    weights = (draw(st_event_rate),)

    xs, ys = make_classification(
        n_samples = n_samples,
        n_features = n_features, n_informative = n_informative, n_redundant = n_redundant,
        n_classes = 2, weights = weights,
        random_state = seed,
        n_clusters_per_class = 1)

    return (xs, ys)


@st.composite
def st_multiclass_problem(
        draw,
        st_n_samples = st_n_samples(),
        st_n_features = st_n_features(),
        st_n_classes = st.integers(min_value = 3, max_value = 10),
        st_event_rate = st.floats(min_value = 0.1, max_value = 0.9),
        seed = None,
        named = False):

    min_n_samples_per_class = 10

    n_clusters_per_class = draw(st.integers(min_value = 2, max_value = 5))
    n_classes = draw(st_n_classes)

    n_samples = draw(st_n_samples)

    min_n_informative = int(np.ceil(np.log(n_clusters_per_class * n_classes)/np.log(2)))

    n_features = max(min_n_informative, draw(st_n_features))
    n_informative = draw(st.integers(min_value = min_n_informative, max_value = n_features))
    n_redundant = draw(st.integers(min_value = 0, max_value = n_features - n_informative))

    weights = draw(st.lists(st_event_rate, min_size = n_classes, max_size = n_classes))

    xs, ys = make_classification(
        n_samples = n_samples,
        n_features = n_features, n_informative = n_informative, n_redundant = n_redundant,
        n_classes = n_classes, weights = weights,
        random_state = seed,
        n_clusters_per_class = n_clusters_per_class)

    vs, cnts = np.unique(ys, return_counts = True)
    cs = vs[cnts < min_n_samples_per_class]

    if len(cs) > 0:
        c0 = vs[cnts >= min_n_samples_per_class][-1]
        ys = np.where(np.isin(ys, cs), c0 , ys)

    return (xs, ys)


@st.composite
def st_regression_problem(
        draw,
        st_n_samples = st_n_samples(),
        st_n_features = st_n_features(),
        seed = None,
        named = False):

    n_samples = draw(st_n_samples)

    n_features = max(1, draw(st_n_features))
    n_informative = draw(st.integers(min_value = 1, max_value = n_features))

    xs, ys = make_regression(
        n_samples = n_samples,
        n_features = n_features, n_informative = n_informative,
        noise = 0.5,
        random_state = seed)

    return (xs, ys)


__all__ = [
    'st_binary_problem',
    'st_multiclass_problem',
    'st_regression_problem']
