from ad_hoc_utils.common.imp_bootstrap import get_compats, import_all_from_submodules

import_all_from_submodules(
    globals(), __name__,
    *get_compats(__file__))
