def _BaseMultimethod_register(self, *args):
    if len(args) == 2:
        [key, handler] = args
        return self._register(key, handler)

    if len(args) == 1:
        [handler] = args
        prefix = self._f.__name__ + '_'
        assert handler.__name__.startswith(prefix), f'name should be like `{prefix}...`'
        return self._register(
            self._name_to_key(handler.__name__[len(prefix):]),
            handler)

    raise Exception('incorrect signature')


__all__ = ['_BaseMultimethod_register']
