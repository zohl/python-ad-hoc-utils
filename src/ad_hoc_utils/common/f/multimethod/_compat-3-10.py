def _BaseMultimethod_register(self, *args):
    match args:
        case [key, handler]:
            return self._register(key, handler)
        case [handler]:
            prefix = self._f.__name__ + '_'
            assert handler.__name__.startswith(prefix), f'name should be like `{prefix}...`'
            return self._register(
                self._name_to_key(handler.__name__[len(prefix):]),
                handler)
        case _:
            raise Exception('incorrect signature')


__all__ = ['_BaseMultimethod_register']
