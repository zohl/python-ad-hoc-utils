from typing import overload, Callable, TypeVar, Any, Iterable, Literal

T = TypeVar('T')
U = TypeVar('U')
V = TypeVar('V')


lift_maybe: (
        Callable[[Callable[[T], U]], Callable[[T | None], U | None]] |
        Callable[[Callable[[Iterable[T]], U]], Callable[[Iterable[T | None]], U | None]])


round_robin_by: Callable[[Callable[[T], U]], Callable[[list[T]], list[T]]]


def head(xs: Iterable[T]) -> T | None: ...
def last(xs: Iterable[T]) -> T | None: ...
