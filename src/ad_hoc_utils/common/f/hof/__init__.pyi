from typing import overload, Callable, TypeVar, Any, Iterable, Literal, ParamSpec, Concatenate


T = TypeVar('T')
U = TypeVar('U')
V = TypeVar('V')
Ps = ParamSpec('Ps')


lcurry: Callable[[Callable[Ps, T]], Callable[Ps, Callable[Ps, T]]]
rcurry: Callable[[Callable[Ps, T]], Callable[Ps, Callable[Ps, T]]]
uncurry: Callable[[Callable[[T], Callable[Ps, U]]], Callable[Concatenate[T, Ps], U]]


def composable(f: Callable[..., T]) -> Callable[..., T]: ...
