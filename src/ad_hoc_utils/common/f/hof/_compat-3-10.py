from functools import wraps, reduce
from .common import swap, identity, spread


def apply(f, /, *args, **kwargs):
    """
    Takes function and it's arguments, then executes it.

    """

    match f:
        case [*fs]: return tuple(map(spread(apply), zip(fs, *args)))
        case _: return f(*args, **kwargs)


def compose(*fs):
    """Functional composition.

    Combines functions into one pipeline. Alias: ``_c``.
    """

    match fs:
        case []:  return identity
        case [f]: return f

    @wraps(fs[-1])
    def result(*args, **kwargs):
        return reduce(swap(apply), reversed(fs[:-1]), fs[-1](*args, **kwargs))

    return result


__all__ = ['apply', 'compose']
