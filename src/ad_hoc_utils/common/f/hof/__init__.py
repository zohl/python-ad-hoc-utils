from typing import overload, Callable, TypeVar, Any, Literal

from functools import wraps

from ad_hoc_utils.common.imp_bootstrap import get_compats, import_all_from_submodules

import_all_from_submodules(
    globals(), __name__,
    'common',
    *get_compats(__file__))


_c = compose


def composable(f):
    @wraps(f)
    def result(*fs):
        return f(compose(*fs))

    return result


_map = composable(lcurry(map))
"Curried map."


_filter = composable(lcurry(filter))
"Curried filter."


_sort = composable(lambda f: lambda xs: sorted(xs, key = f))
"Curried sort."


__all__ += [
    '_c', 'composable',
    '_map', '_filter', '_sort']
