lcurry = (
    lambda f, /:
    lambda *bound_args, **bound_kwargs:
    lambda *args, **kwargs:
    f(*bound_args, *args, **(bound_kwargs | kwargs)))
"""Left-side currying.

Binds arguments in ``f`` from left to right. Alias: ``_l``. Example:

>>> _l(op.sub, 10)(4)
6
"""


rcurry = (
    lambda f, /:
    lambda *bound_args, **bound_kwargs:
    lambda *args, **kwargs:
    f(*args, *bound_args, **(kwargs | bound_kwargs)))
"""Right-side currying.

Binds arguments in ``f`` from right to left. Alias: ``_r``. Example:

>>> _l(op.sub, 10)(4)
-6
"""


uncurry = (
    lambda f, /:
    lambda x, /, *args, **kwargs:
    f(x)(*args, **kwargs))
"""Uncurry the function.

"Flattens" a high-order function by one level.
"""


_l = uncurry(lcurry)
_r = uncurry(rcurry)


identity = lambda _x: _x
"Id function."


constant = lambda x: lambda *args, **kwargs: x
"Constant function."


swap = lambda f, /: lambda *args: f(*reversed(args))
"""Inverses the order of positional arguments in ``f``."""


spread = lambda f: lambda args = [], kwargs = {}: f(*args, **kwargs)
"""Modifies the function ``f`` so it takes a single array instead of multiple arguments.

>>> from itertools import chain
>>> lists = [[i]*i for i in range(5)]
>>> list(spread(chain)(lists)) == list(chain(*lists))
True

>>> list(map(spread(lambda x, y: x**2 - y), [(0, 0), (1, 0), (0, 1), (1, 1)]))
[0, 1, -1, 0]

"""


unspread = lambda f: lambda *args: f(args)
"""Modifies the function ``f`` so it takes multiple arguments instead of a single list.

>>> unspread(sum)(1,2,3,4)
10

"""


__all__ = [
    'lcurry', 'rcurry', 'uncurry', '_l', '_r',
    'identity', 'constant', 'swap', 'spread', 'unspread']
