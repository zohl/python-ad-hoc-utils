from functools import wraps, reduce
from .common import swap, identity

def apply(f, /, *args, **kwargs):
    """
    Takes function and it's arguments, then executes it.

    """

    if isinstance(f, list):
        fs = f
        return tuple(map(spread(apply), zip(fs, *args)))

    return f(*args, **kwargs)


def compose(*fs):
    """Functional composition.

    Combines functions into one pipeline. Alias: ``_c``.
    """

    if len(fs) == 0:
        return identity

    if len(fs) == 1:
        [f] = fs
        return f

    @wraps(fs[-1])
    def result(*args, **kwargs):
        return reduce(swap(apply), reversed(fs[:-1]), fs[-1](*args, **kwargs))

    return result


__all__ = ['apply', 'compose']
