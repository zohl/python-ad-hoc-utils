from functools import wraps
from ad_hoc_utils.common.f.hof import _r

def with_optional_params(decorator):
    @wraps(decorator)
    def result(*args, **kwargs):
        if (len(args) == 1) and callable(args[0]) and (len(kwargs) == 0):
            return decorator(args[0])

        return _r(decorator, *args, **kwargs)

    return result


__all__ = ['with_optional_params']
