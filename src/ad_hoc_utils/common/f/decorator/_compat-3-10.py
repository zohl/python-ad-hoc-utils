from functools import wraps
from ad_hoc_utils.common.f.hof import _r

def with_optional_params(decorator):
    @wraps(decorator)
    def result(*args, **kwargs):
        match (args, kwargs):
            case ([f], {}) if callable(f): return decorator(f)
            case _: return _r(decorator, *args, **kwargs)

    return result


__all__ = ['with_optional_params']
