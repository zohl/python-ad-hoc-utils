from ad_hoc_utils.common.imp_bootstrap import get_compats, import_all_from_submodules

from typing import overload, Callable, TypeVar, Any, Iterable, Literal

import inspect
import asyncio
import operator as op
from functools import wraps, reduce, partial
from itertools import zip_longest, chain, groupby
from abc import ABCMeta, abstractmethod
from collections import defaultdict
from collections.abc import Iterable
import logging

from .multimethod import _BaseMultimethod_register

from ad_hoc_utils.common.imp_bootstrap import import_all_from_submodules

import_all_from_submodules(
    globals(), __name__,
    'hof',
    'decorator')


T = TypeVar('T')
U = TypeVar('U')
V = TypeVar('V')


is_none: Callable[[Any], bool] = lambda x: x is None
"""Tests whether ``x`` is None."""


is_just: Callable[[Any], bool] = lambda x: x is not None
"""Tests whether ``x`` is not None."""


lift_maybe = (
    lambda f:
    lambda *xs: None if any(map(is_none, xs)) else f(*xs))
""" Returns ``None`` if so the argument, otherwise applies ``f`` to it."""


round_robin_by = lambda f: _c(
    _filter(is_just), spread(chain), spread(zip_longest),
    _map(snd), _map(second(list)), _r(groupby, _c(f)))
"""Groups elements by ``f`` and takes one element from each one. The initial order is preserved within each group."""


@overload
def select_by(
        cmp: Callable[[U, U], bool],
        f: Callable[[T], U],
        xs: Iterable[T]) -> T: ...

@overload
def select_by(
        cmp: Callable[[U, U], bool],
        f: Callable[[T], U],
        xs: Iterable[T],
        *, return_metric: Literal[False]) -> T: ...

@overload
def select_by(
        cmp: Callable[[U, U], bool],
        f: Callable[[T], U],
        xs: Iterable[T],
        *, return_metric: Literal[True]) -> tuple[T, U]: ...

def select_by(cmp, f, xs, *, return_metric = False):
    x_best = None
    m_best = None
    for x in xs:
        m = f(x)
        if (m_best is None) or cmp(m, m_best):
            x_best = x
            m_best = m
    return (x_best, m_best) if return_metric else x_best


max_by = _l(select_by, lift_maybe(op.gt))


min_by = _l(select_by, lift_maybe(op.lt))


def void(f: Callable[..., T]) -> Callable[..., None]:
    """Executes ``f`` and ignores the returned value."""

    @wraps(f)
    def result(*args, **kwargs):
        f(*args, **kwargs)
    return result


def bypass(*fs):
    """ Executes functions from ``fs`` on a given arguments, but returns the same argument. """

    def result(x):
        for f in fs:
            f(x)
        return x
    return result


def named(name: str, f: Callable[..., T]) -> Callable[..., T]:
    """Gives functoin ``f`` a name

    A workaround to pass lambdas to name-sensitive functions. This function *mutates* ``f``.

    """
    f.__name__ = name
    if hasattr(f, '__qualname__'):
        f.__qualname__ = '.'.join([*f.__qualname__.split('.')[:-1], name])
    return f


fanout = lambda *fs: lambda *args, **kwargs: _c(list, _map(lambda f: f(*args, **kwargs)))(fs)
"""
Applies the same set of arguments to each function from a list.
"""


# TODO: use multimethod

def update(k, f):
    """Applies ``f`` to the element at position ``k`` in the given collection. Does not mutate the collection.
    """

    def result(xs):
        if isinstance(xs, list):
            return [f(x) if i == k else x for i, x in enumerate(xs)]

        if isinstance(xs, tuple):
            return tuple([f(x) if i == k else x for i, x in enumerate(xs)])

        if isinstance(xs, dict):
            return {k0: (f(v) if k0 == k else v) for k0, v in xs.items()}

    return result


def mutate(k, f):
    """Applies ``f`` to the element at position ``k`` in the given collection and replaces it.
    """

    def result(xs):
        xs[k] = f(xs[k])
        return xs

    return result


first = composable(lambda f: update(0, f))
"""Applies ``f`` to the first element in the collection.

A special case of Haskell arrow's `first`.
"""


second = composable(lambda f: update(1, f))
"""Applies ``f`` to the second element in the collection.

A special case of Haskell arrow's `second`.
"""


_if = lambda p, t, f = None: \
    lambda *args, **kwargs: \
    t(*args, **kwargs) if p(*args, **kwargs) else lift_maybe(_r(apply, *args, **kwargs))(f)
""" Ternary functional ``if``."""


def cond(x, *cs):
    for (k, v) in cs:
        if k(x):
            return v


def condp(x, p, *cs):
    for (k, v) in cs:
        if p(k, x):
            return v


def throw(e):
    raise e


class LMetaGen(type):
    """Metaclass for ``LMeta`` and ``PMeta``."""

    def __new__(mcls, name, bases, namespace, ops = {}, proxy_ops = [], **kwargs):
        r = type.__new__(mcls, name, bases, dict(kwargs))
        for k, v in ops.items():
            setattr(r, f'__{k}__', v)
        for a in proxy_ops:
            LMetaGen.proxy_op(r, a)
        return r

    @staticmethod
    def proxy_op(r, a):
        setattr(r, f'__{a}__', lambda self, *args, **kwargs: lambda x: getattr(x, f'__{a}__')(*args, **kwargs))


class LMeta(
    type,
    metaclass = LMetaGen,
    ops = dict(
        getattr = (
            lambda cls, name:
            lambda *args, **kwargs: _c(
                _if(callable, _r(apply, *args, **kwargs), identity), _r(getattr, name)))),
    proxy_ops = [
        'and', 'or', 'xor',
        'rand', 'ror', 'rxor',
        'not', 'inv', 'invert',

        'add', 'sub', 'mul', 'matmul', 'pow', 'mod', 'floordiv', 'truediv',
        'radd', 'rsub', 'rmul', 'rmatmul', 'rpow', 'rmod', 'rfloordiv', 'rdivmod', 'rtruediv',
        'neg', 'pos',

        'eq', 'ne', 'gt', 'ge', 'lt', 'le',
        'lshift', 'rshift',
        'rlshift', 'rrshift',

        'concat',
        'getitem', 'call',
        'abs']):
    """Metaclass for ``L``."""

    pass


class PMeta(
    type,
    metaclass = LMetaGen,
    ops = dict(
        getattr = lambda cls, name: lambda x: getattr(x, name))):
    """Metaclass for ``P``."""

    pass


class P_Meta(
    type,
    metaclass = LMetaGen,
    ops = dict(
        getattr = lambda cls, name: lambda x: getattr(x, name) if hasattr(x, name) else None)):
    """Metaclass for ``P_``."""

    pass


class L(metaclass = LMeta):
    """Lambda generator, see ``LMeta`` arguments for supported operations."""

    pass


fst: Callable[[tuple[T, U]], T] = L[0]  # type: ignore

snd: Callable[[tuple[T, U]], U] = L[1]  # type: ignore


class P(metaclass = PMeta):
    """Lambda generator for objects' properties only."""

    pass


class P_(metaclass = P_Meta):
    """Lambda generator for objects' properties only, returns `None` when the attribute is not present."""

    pass


def ignore_unused(f):
    """Allows function to accept arguments that is not mentioned in it's signature."""

    has_var_args = False
    has_var_kwargs = False
    kwargs_only = set()
    kwargs_positional = set()
    n_positional_only = 0

    for (k, v) in list(inspect.signature(f).parameters.items()):
        if v.kind == inspect.Parameter.VAR_KEYWORD:
            has_var_kwargs = True
        elif v.kind == inspect.Parameter.VAR_POSITIONAL:
            has_var_args = True
        elif v.kind == inspect.Parameter.KEYWORD_ONLY:
            kwargs_only.add(k)
        elif v.kind == inspect.Parameter.POSITIONAL_ONLY:
            n_positional_only += 1
        elif v.kind == inspect.Parameter.POSITIONAL_OR_KEYWORD:
            kwargs_positional.add(k)

    @wraps(f)
    def result(*args0, **kwargs0):
        return f(
            *(args0
              if has_var_args
              else args0[:n_positional_only + len(kwargs_positional - set(kwargs0.keys()))]),
            **(kwargs0
               if has_var_kwargs
               else select_keys(
                   kwargs0,
                   kwargs_only.union(kwargs_positional))))
    return result


def select_keys(d, keys):
    """Returns subset of a dictionary."""
    return {k: d[k] for k in keys if k in d}


def iterate(f, x):
    """
    Returns a sequence of approximations to fixed point of ``f`` starting with ``x``.

    """

    while x is not None:
        yield x
        x = f(x)


def safe_next(it):
    """
    Same as ``next``, but suppresses ``StopIteration`` exception.

    """

    try:
        return next(it)
    except StopIteration:
        pass


def suppress_exceptions(f, do_warn = True):
    @wraps(f)
    def result(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            if do_warn:
                logging.warning(str(e))
    return result


def retry(max_attempts, on):

    if (not isinstance(on, set)) and (not isinstance(on, list)):
        on = set([on])

    def decorator(f):
        @wraps(f)
        def result(*args, **kwargs):
            n_attempts = max_attempts

            while n_attempts > 0:
                try:
                    return f(*args, **kwargs)
                except Exception as e:
                    n_attempts -= 1
                    if (type(e) not in on) or n_attempts == 0:
                        raise e
                    logging.warning(f'caught {type(e).__name__} in {f.__name__}, remaining attempts: {n_attempts}')
        return result

    return decorator


def head(xs):
    return safe_next(iter(xs))


def last(xs):
    return head(reversed(xs))


def tail(xs: Iterable[T]) -> Iterable[T]:
    it = iter(xs)
    safe_next(it)
    return it



cumsum: Callable[[Iterable[T]], T] = lambda xs: _c(
    _map(fst), iterate)(
    spread(
        lambda acc, xs: _c(
            lift_maybe(lambda x: (acc + x, xs)),
            safe_next)(xs)),
    (0, iter(xs)))


is_monotonic: Callable[[list[T]], bool] = lambda xs: _c(
    L <= 1, len, set,
    _map(spread(op.ge)),
    _filter(spread(op.ne)), zip)(xs, tail(xs))


class Thunk():
    """Lazy evaluation wrapper.

    Takes function and it's arguments, yields and cache result on ``__call__``.

    Example:

    >>> v = thunk(lambda x, y: x + y, 12, 34)
    >>> v()
    46

    """

    def __init__(self, f, *args, **kwargs):
        self._f = f
        self._args = args
        self._kwargs = kwargs
        self._is_realized = False

    def is_realized(self):
        return self._is_realized

    def __call__(self):
        if not self.is_realized():
            self._value = self._f(*self._args, **self._kwargs)
            self._is_realized = True
        return self._value

    def prepend(self, *args, **kwargs):
        return Thunk(self._f, *args, *self._args, **{**kwargs, **self._kwargs})

    def append(self, *args, **kwargs):
        return Thunk(self._f, *self._args, *args, **{**self._kwargs, **kwargs})


def thunk(f, *args, **kwargs):
    """Lazy evaluation wrapper.

    Same as ``Thunk``, except the lowercased name.

    """

    return Thunk(f, *args, **kwargs)


def thread(x, *fs, mode = 'prepend', exit_on_none = False):
    r = x
    extend = getattr(Thunk, mode)

    for f in fs:
        if r is None: return
        r = extend(f, r)() if isinstance(f, Thunk) else f(r)
    return r

def get_in(x, *fs):
    return thread(x, *fs, exit_on_none = True)


def call_counter(f):
    @wraps(f)
    def result(*args, **kwargs):
        f._n_calls += 1
        return f(*args, **kwargs)

    setattr(f, '_n_calls', 0)
    setattr(result, 'get_n_calls', lambda: f._n_calls)

    return result


def cached(key = lambda *args, **kwargs: (args, _c(tuple, _sort(fst))(kwargs.items()))):
    def decorator(f):
        @wraps(f)
        def result(*args, **kwargs):
            k = key(*args, **kwargs)
            if k not in f._cache:
                f._cache[k] = f(*args, **kwargs)
            return f._cache[k]

        setattr(f, '_cache', dict())
        return result

    return decorator


# TODO: proxy decorator: factor out as another decorator
# for attr in ['__getitem__', '__contains__', 'keys']:
#     setattr(RegisterDecorator, attr, (
#         lambda attr_:
#         lambda self, *args, **kwargs: getattr(self._storage, attr_)(*args, **kwargs))(attr))
#

function = type(lambda: ())

ellipsis = type(...)

none_type = type(None)


def wrap_async(f):
    """Convert synchronous function ``f`` to asynchronous one."""

    @wraps(f)
    async def run(*args, loop = None, executor = None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()
        pf = partial(f, *args, **kwargs)
        return await loop.run_in_executor(executor, pf)
    return run


def wrap_sync(f):
    """Convert asynchronous function ``f`` to synchronous one."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        if asyncio.iscoroutine(res):
            return asyncio.get_event_loop().run_until_complete(res)
        return res
    return wrapper



def with_callbacks(*names, default = None):
    """
    Defines methods to set callbacks.

    """

    def decorator(cls):
        for name in names:
            setattr(
                cls, f'on_{name}',
                (lambda name:
                 lambda self, callback: setattr(self, f'_on_{name}', callback))(name))

            if not hasattr(cls, f'_on_{name}'):
                setattr(cls, f'_on_{name}', default)

        return cls
    return decorator


mixin_class_match = lambda base: base.__name__.endswith('Mixin')


def wrap_mixin_method(cls, bases, method):

    f = getattr(cls, method)

    @wraps(f)
    def result(*args, **kwargs):
        for base in bases:
            if hasattr(base, method):
                getattr(base, method)(*args, **kwargs)
        return f(*args, **kwargs)

    return result


def mixins(methods = ['__init__'], match = mixin_class_match):
    """
    Combines ``methods`` of parent classes (as specified by ``match``).

    """

    def decorator(cls):
        bases = _c(list, _filter(match))(cls.__bases__)
        for method in methods:
            setattr(cls, method, wrap_mixin_method(cls, bases, method))
        return cls

    return decorator




def bind_parameters(f, args, kwargs):
    s = inspect.signature(f)
    bindings = s.bind(*args, **kwargs)
    bindings.apply_defaults()
    return bindings


def summarize_signature(s):
    d = _c(_l(defaultdict, lambda: []), _map(second(list)), _r(groupby, P.kind), list)(s.parameters.values())
    return dict(
        positional = _c(
            list, _map(P.name))([
            *d[inspect.Parameter.POSITIONAL_ONLY],
            *d[inspect.Parameter.POSITIONAL_OR_KEYWORD]]),

        keyword = _c(
            list, _map(P.name))([
            *d[inspect.Parameter.KEYWORD_ONLY]]),

        var_positional = _c(
            lambda x: x[0] if x else None, list, _map(P.name))(
            d[inspect.Parameter.VAR_POSITIONAL]),

        var_keyword = _c(
            lambda x: x[0] if x else None, list, _map(P.name))(
            d[inspect.Parameter.VAR_KEYWORD]))


def restrict_bindings(signature, bindings):
    s0 = summarize_signature(bindings.signature)
    s = summarize_signature(signature)

    args, kwargs = [], dict()

    args0 = (
        []
        if not s0['var_positional']
        else bindings.arguments[s0['var_positional']])

    kwargs0 = (
        dict()
        if not s0['var_keyword']
        else bindings.arguments[s0['var_keyword']])

    for p0 in s0['positional']:
        if len(args) == len(s['positional']):
            break
        if p0 == s['positional'][len(args)]:
            args.append(bindings.arguments[p0])

    positional_diff = len(s['positional']) - len(args)
    args += args0[:(None if s['var_positional'] else positional_diff)]

    kwargs = (
        select_keys(bindings.arguments, s['keyword']) |
        (kwargs0
        if s['var_keyword']
        else select_keys(kwargs0, s['keyword'])))

    return (args, kwargs)


def apply_bindings(f, bindings):
    args, kwargs = restrict_bindings(inspect.signature(f), bindings)
    return f(*args, **kwargs)


class BaseMultimethod(metaclass = ABCMeta):
    @staticmethod
    def _name_to_key(s):
        return s

    @staticmethod
    def _bindings_to_key(bindings, key):
        if callable(key):
            return apply_bindings(key, bindings)
        if type(key) == str:
            return bindings.arguments[key]
        if type(key) == tuple:
            return tuple([bindings.arguments[k] for k in key])

    def __init__(self, f, *, key, force = False, **kwargs):
        self._f = f
        self._key = key
        self._force = force
        self._handlers = dict()

        for k, v in select_keys(kwargs, ['name_to_key']).items():
            setattr(self, '_' + k, v)

        self.__name__ = f.__name__
        self.__signature__ = inspect.signature(f)
        self._update_doc()

    def __getattr__(self, arg):
        print(arg)

    def _update_doc(self):
        self.__doc__ = '\n'.join([
            (self._f.__doc__ or ''),
            '',
            'Available instances:',
            '--------------------',
            '',
            *[f'- {k}: ' + _c(str, inspect.signature)(v) + '\n' + (v.__doc__ or 'no description') + '\n'
              for k, v in self._handlers.items()]])

    def _register(self, key, handler):
        exists = key in self._handlers
        if exists:
            msg = f'overwriting handler for {self.__name__}::{key}'
            if self._force:
                logging.warning(msg)
            else:
                raise Exception(msg)

        self._handlers[key] = handler
        self._update_doc()

        return handler


    def register(self, *args):
        return _BaseMultimethod_register(self, *args)

    @abstractmethod
    def _bind(self, *args, **kwargs):
        pass

    @abstractmethod
    def _call(self, h, args, kwargs, bindings = None):
        pass

    def __call__(self, *args, **kwargs):
        bindings = self._bind(*args, **kwargs)
        k = self._bindings_to_key(bindings, self._key)
        return self._call(self._handlers[k], args, kwargs, bindings)

    def __getattr__(self, key):
        if key in self._handlers:
            return lambda *args, **kwargs: self._call(self._handlers[key], args, {self._key: key}, kwargs)
        else:
            raise AttributeError

@with_optional_params
class Multimethod(BaseMultimethod):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _bind(self, *args, **kwargs):
        return bind_parameters(self._f, args, kwargs)

    def _call(self, h, args, kwargs, bindings = None):
        return apply_bindings(h, bindings or self._bind(*args, **kwargs))


@with_optional_params
class WrappedMultimethod(BaseMultimethod):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _bind(self, *args, **kwargs):
        return bind_parameters(self._f, [None, *args], kwargs)

    def _call(self, h, args, kwargs, bindings = None):
        return self._f(h, *args, **kwargs)

multimethod = Multimethod
wrapped_multimethod = WrappedMultimethod



class Patch:
    def __init__(self, x, **kwargs):
        self._x = x
        self._new = kwargs
        self._old = {
            k: getattr(x, k)
            for k in kwargs.keys()
            if hasattr(x, k)}

    def __enter__(self):
        for k, v in self._new.items():
            setattr(self._x, k, v)

    def __exit__(self, exc_type, exc_value, traceback):
        for k in self._new.keys():
            if k in self._old:
                setattr(self._x, k, self._old[k])
            else:
                delattr(self._x, k)

patch = Patch


__all__ += [
    'is_none', 'is_just', 'lift_maybe',
    'round_robin_by',
    'select_by', 'max_by', 'min_by',
    'void', 'bypass', 'named', 'ignore_unused',
    'mutate',
    'fanout', 'first', 'second',
    '_if', 'cond', 'condp',
    'throw',
    'L', 'fst', 'snd', 'P', 'P_',
    'bind_parameters', 'summarize_signature', 'restrict_bindings', 'apply_bindings',
    'multimethod', 'wrapped_multimethod',
    'select_keys',
    'iterate', 'safe_next', 'suppress_exceptions',
    'head', 'tail', 'last',
    'is_monotonic',
    'cumsum',
    'Thunk', 'thunk',
    'thread', 'get_in',
    'call_counter', 'cached',
    'function', 'ellipsis', 'none_type',
    'wrap_async', 'wrap_sync',
    'with_callbacks', 'wrap_mixin_method', 'mixins',
    'patch']



