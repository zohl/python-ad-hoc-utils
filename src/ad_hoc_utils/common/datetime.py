from typing import Callable
from datetime import datetime, timedelta, time


def timestamps_between(b: time, dt_from: datetime, dt_to: datetime) -> int:
    offset = timedelta(minutes = b.hour * 60 + b.minute)
    return ((dt_to - offset).date() - (dt_from - offset).date()).days


last_day: Callable[[datetime], datetime] = lambda dt: (
    (dt.replace(day = 1) + timedelta(days = 32)).replace(day = 1) -
    timedelta(days = 1))


trunc_day: Callable[[datetime], datetime] = lambda dt: datetime.combine(dt.date(), time())


__all__ = [
    'timestamps_between',
    'last_day', 'trunc_day']
