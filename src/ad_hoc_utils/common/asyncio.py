from collections import deque
from functools import wraps
import asyncio
import time


async def fmap_async(f, x):
    r = await x
    return f(r)


def rate_limit(n_calls, period, label = None):
    def decorator(f):
        ts = deque()
        ls = deque()

        @wraps(f)
        async def result(*args, **kwargs):
            lock = asyncio.Event()
            ls.append(lock)
            if len(ls) > 1:
                await ls.popleft().wait()

            t = time.monotonic_ns()
            while (len(ts) > 0) and (t - ts[0] > int(10**9 * period)):
                ts.popleft()

            if len(ts) >= n_calls:
                dt = (ts[-n_calls] - time.monotonic_ns() + int(period * 10**9))
                await asyncio.sleep(dt*1.0/10**9)

                t = time.monotonic_ns()
                while (len(ts) > 0) and (t - ts[0] > int(10**9 * period)):
                    ts.popleft()

            ts.append(time.monotonic_ns())
            lock.set()
            return await f(*args, **kwargs)

        return result
    return decorator


__all__ = [
    'rate_limit']
