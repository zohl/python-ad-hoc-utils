from typing import Callable, Union, Optional, List

import logging

from pathlib import Path
import base64
from functools import wraps, partial
from getpass import getpass
import datetime
from bs4 import BeautifulSoup
import requests
import textwrap

import sqlalchemy as sql
import sqlalchemy.types as sqlt
from sqlalchemy import create_engine, Table, Column
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.schema import CreateTable, DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.sql.dml import Executable
from sqlalchemy.sql.expression import BindParameter, FunctionElement


import operator as op
from collections import OrderedDict

from ad_hoc_utils.common.f import L, wrapped_multimethod, P, P_, _r, _c, _filter, _map, _sort, fanout, ignore_unused, is_just, fst, snd
from ad_hoc_utils.common.f import lift_maybe, named, spread, void, first, identity, none_type
from ad_hoc_utils.common.config import BaseFetcher

from itertools import chain, repeat



def get_connection_string(db_type: str) -> str:
    return base64.b64decode(
        (Path(__file__).parent / 'data' / f'{db_type}-connection-string.b64')
        .read_bytes()).decode('utf8')

_engine_default_args = dict(future = True)


@wrapped_multimethod(key = 'db_type')
def connect_to_db(
        callback, db_type, *,
        asyncio = False,
        password = None,
        **kwargs):
    """Creates a DB engine.

    A multimethod function that dispatches on ``db_type`` argument. For
    database-specific arguments see ``connect_to_db_*`` definitions.
    """

    mk_engine = (
        create_async_engine
        if asyncio
        else create_engine)

    password_value = (
        password(db_type = db_type, **kwargs)
        if isinstance(password, BaseFetcher)
        else password)

    (connection_args, engine_args) = callback(
        asyncio = asyncio,
        password = password_value,
        **kwargs)

    return mk_engine(
        get_connection_string(db_type).format(**connection_args),
        **(_engine_default_args | engine_args))


def sql_exec(tx: Connection, query: Union[str, Executable], params = None, *, mogrify: bool = False):
    """Executes ``query`` with given arguments.

    ``query`` can be either ``sql.text`` (as required by ``SQLAlchemy >= 2.0.0``) or ``str``.

    ``params`` follows the conventions of ``Connection.execute``, i.e. one-time
    run for a dictionary, execmany for list of dictionaries.
    """

    if params is None:
        params = dict()

    execute_many = isinstance(params, list)

    sample_params = None
    if execute_many:
        if len(params) > 0:
            sample_params = params[0]
    else:
        sample_params = params

    stmt = sql.text(query).params(sample_params) if type(query) == str else query

    if mogrify:
        tmpl = str(stmt.compile(dialect = tx.engine.dialect.get_dialect_cls('')(paramstyle = 'pyformat')))

        msg = _c(' '.join, _filter(is_just))([
            'DB >>>',
            tmpl
            if (sample_params is None)
            else (tmpl % {k: f'"{v}"' for k, v in sample_params.items()}),

            None
            if (not execute_many) or (len(params) == 0)
            else f'+ {len(params) - 1} more'])

        logging.info(msg)

    return (None if (execute_many and len(params) == 0) else tx.execute(stmt, params))



__all__ = [
    'connect_to_db',
    'sql_exec']
