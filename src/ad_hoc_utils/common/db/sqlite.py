from typing import Callable, Optional, List

import logging

from pathlib import Path
import base64
from functools import wraps, partial
from getpass import getpass
import datetime
from bs4 import BeautifulSoup
import requests
import textwrap
import urllib

import sqlalchemy as sql
import sqlalchemy.types as sqlt
from sqlalchemy import create_engine, Table, Column
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.ext.asyncio import create_async_engine, AsyncEngine
from sqlalchemy.schema import CreateTable, DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.dml import Executable
from sqlalchemy.sql.expression import BindParameter, FunctionElement


import operator as op
from collections import OrderedDict

from ad_hoc_utils.common.f import L, P, P_, _r, _c, _filter, _map, _sort, fanout, ignore_unused, is_just, fst, snd
from ad_hoc_utils.common.f import lift_maybe, named, spread, void, first, identity, none_type
from itertools import chain, repeat

from .common import connect_to_db


@connect_to_db.register
def connect_to_db_sqlite(
        *,
        asyncio = False,
        path = None,
        password = None,
        **kwargs):
    """Creates SQLite DB engine.

    Connection-specific arguments: ``path``.
    Depending on ``asyncio`` uses ``pysqlite`` or ``aiosqlite`` driver.
    """

    return (
        dict(
            driver = ('aiosqlite' if asyncio else 'pysqlite'),
            path = ('/' + path if path is not None else '')),
        kwargs)


__all__ = []
