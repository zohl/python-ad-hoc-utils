from ad_hoc_utils.common.imp import extras

extras.require_deps(__name__, 'postgres')


from typing import Callable, Optional, List

import logging

from pathlib import Path
import base64
from functools import wraps, partial
from getpass import getpass
import datetime
from bs4 import BeautifulSoup
import requests
import textwrap
import urllib

import sqlalchemy as sql
import sqlalchemy.types as sqlt
from sqlalchemy import create_engine, Table, Column
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.ext.asyncio import create_async_engine, AsyncEngine
from sqlalchemy.schema import CreateTable, DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.dml import Executable
from sqlalchemy.sql.expression import BindParameter, FunctionElement


import operator as op
from collections import OrderedDict

from ad_hoc_utils.common.f import L, P, P_, _r, _c, _filter, _map, _sort, fanout, ignore_unused, is_just, fst, snd
from ad_hoc_utils.common.f import lift_maybe, named, spread, void, first, identity, none_type
from itertools import chain, repeat

from .common import connect_to_db


logging.getLogger('sqlalchemy.dialects.postgresql').setLevel(logging.INFO)


@connect_to_db.register
def connect_to_db_postgres(
        *,
        asyncio: bool = False,
        host: str,
        port: int = 5432,
        db_name,
        user: str,
        password: str,
        **kwargs):
    """Creates PostgreSQL DB engine.

    Connection-specific arguments: ``host``, ``db_name``, ``user``,
    ``password``. If password is not specified, ``.pgpass`` will be used.

    Depending on ``asyncio`` uses ``psycopg2`` or ``asyncpg`` driver.
    """

    return (
        dict(
            driver = ('asyncpg' if asyncio else 'psycopg2'),
            host = host,
            db_name = db_name,
            user = user,
            password = urllib.parse.quote(password or '')),
        kwargs)



def fetch_pg_reserved_words() -> List[str]:
    """Fetches the latest version of reserved words from the official documentation."""

    page = BeautifulSoup(
        requests.get('https://www.postgresql.org/docs/current/sql-keywords-appendix.html').content,
        'html.parser')
    dom_table_contents = page.select_one('.table-contents')
    return ([]
            if dom_table_contents is None
            else list(map(lambda x: x.text.lower(), dom_table_contents.select('td:first-child code'))))


pg_reserved_words = (Path(__file__).parent / 'data' / 'postgres-reserved-words.txt').read_text().split('\n')
"""Reserved PostreSQL words that might cause problems when used as entity names."""


pg_type_size = {
    sqlt.SmallInteger: 2,
    sqlt.Integer: 4,
    sqlt.BigInteger: 8,
    sqlt.Float: 8,
    sqlt.DateTime: 8,
    sqlt.Date: 4,
    sqlt.String: 1,
    sqlt.Numeric: 1}


def sql_pg_padding_loss(columns: List[Column], word_size: int = 8) -> int:
    """Calculates excessive usage of space (in bytes) per row."""

    loss = 0
    offset = 0

    for sz in _map(pg_type_size.__getitem__, type, P.type)(columns):  # type: ignore
        if offset + sz > word_size:
            loss += word_size - offset
            offset = 0
        offset = (offset + sz) % word_size

    if offset > 0:
        loss += word_size - offset

    return loss


def sql_pg_align_columns(columns: List[Column], word_size: int = 8) -> List[Column]:
    """Reorders columns to minimize padding loss."""

    return _c(
        list,
        _map(spread(Column)),
        _sort(op.neg, pg_type_size.__getitem__, type, L[1]),  # type: ignore
        _map(fanout(P.name, P.type)))(columns)                # type: ignore


__all__ = [
    'fetch_pg_reserved_words',
    'sql_pg_padding_loss', 'sql_pg_align_columns']
