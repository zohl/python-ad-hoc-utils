from ad_hoc_utils.common.imp import extras

extras.require_deps(__name__, 'ml')


from typing import Callable, Union, Optional, List

import logging
from asyncio import iscoroutine

from pathlib import Path
import base64
from functools import wraps, partial
from getpass import getpass
import datetime
from bs4 import BeautifulSoup
import requests
import textwrap
import urllib.parse

import sqlalchemy as sql
import sqlalchemy.types as sqlt
from sqlalchemy import create_engine, Table, Column
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.schema import CreateTable, DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.dml import Executable
from sqlalchemy.sql.expression import BindParameter, FunctionElement


import operator as op
from collections import OrderedDict

from ad_hoc_utils.common.asyncio import fmap_async
from ad_hoc_utils.common.f import L, wrapped_multimethod, multimethod, P, P_, _r, _c, _filter, _map, _sort, fanout, ignore_unused, is_just, fst, snd
from ad_hoc_utils.common.f import lift_maybe, named, spread, void, first, identity, none_type
from itertools import chain, repeat

import numpy as np
import pandas as pd

from .common import sql_exec

with extras.modules('tqdm'):
    from tqdm import tqdm

with extras.deps(__name__, 'postgres'):
    from .postgres import sql_pg_align_columns, sql_pg_padding_loss


def sql_load_to_pandas_postprocess(r, coerce_dates: bool = True):
    columns = _c(list, _map(str.lower, fst))(r.cursor.description)
    rows = r.mappings().fetchall()
    if len(rows) == 0:
        return pd.DataFrame(data = [], columns = columns)

    df = pd.DataFrame.from_records(rows, coerce_float = True)

    types = dict()
    for c in df.columns:
        if df[c].dtype == object:
            if coerce_dates and df[c].apply(type).isin({datetime.date, datetime.datetime}).any():
                types[c] = 'datetime64[ns]'

    return df.astype(types)


def sql_load_to_pandas(
        tx: Connection,
        query: Union[str, Executable],
        params = None,
        *, mogrify: bool = False,
        **kwargs):
    """Loads ``query`` results into a pandas dataframe.

    A drop-in replacement for ``pandas.DataFrame.read_sql`` to support newer versions of SQLAlchemy.

    Instead of

    >>> df = pd.read_sql('select * from table_name', db)

    use it like this:

    >>> df = None
    >>> with db.begin() as tx:
    >>>    df = sql_load_to_pandas(tx, 'select * from table_name')
    """

    r = sql_exec(tx, query, params, mogrify = mogrify)

    return (
        fmap_async(_r(sql_load_to_pandas_postprocess, **kwargs), r)
        if iscoroutine(r)
        else sql_load_to_pandas_postprocess(r, **kwargs))


@wrapped_multimethod(key = lambda t: str(t).lower().split('(')[0])
def sql_coerce_type(callback, t, x):
    return (None if is_null(x) else callback(x))

_c(list, _map(spread(sql_coerce_type.register)))([
    ('bigint', lambda x:None if np.isnan(x) else int(x)),
    ('integer', lambda x: None if np.isnan(x) else int(x)),
    ('smallint', lambda x: None if np.isnan(x) else int(x)),
    ('float', lambda x: None if np.isnan(x) else float(x)),
    ('double_precision', lambda x: None if np.isnan(x) else float(x)),
    ('varchar', lambda x: x),
    ('text', lambda x: x),
    ('date', lambda x: None if x is pd.NaT else x),
    ('datetime', lambda x: None if x is pd.NaT else x),
    ('timestamp', lambda x: None if x is pd.NaT else x),
    ('boolean', lambda x: None if np.isnan(x) else bool(x))])


def sql_dump_from_pandas(tx: Connection, df: pd.DataFrame, table: Table, *, mogrify: bool = False):
    """Writes ``df`` contents into a database table.

    An rough equivalent of ``pandas.DataFrame.to_sql``, but uses different mechanics for type coercion.
    """

    table_columns = set(table.columns.keys())
    df_columns = set(df.columns)

    extra_table_columns = list(table_columns - df_columns)
    extra_df_columns = list(df_columns - table_columns)

    if len(extra_table_columns) > 0:
        logging.warning('`table` columns will not be filled: ' + ', '.join(extra_table_columns))

    if len(extra_df_columns) > 0:
        logging.warning('`df` columns will not be used: ' + ', '.join(extra_df_columns))

    columns = list(table_columns.intersection(df_columns))

    sql_exec(
        tx, sql.insert(table),
        [dict(map(lambda x:
                  (x[0], sql_coerce_type(table.columns[x[0]].type, x[1])),
                  zip(columns, r)))
         for r in df[columns].itertuples(index = False)], mogrify = mogrify)


def sql_dump_from_pandas_chunked(
        db: Engine,
        df: pd.DataFrame,
        table: sql.Table,
        chunk_size: int = 10000, *, mogrify: bool = False, verbose: bool = True):
    """Writes ``df`` contents into a database table by chunks.

    Same as ``sql_dump_from_pandas`` but shows the progress and commits after writing each chunk.
    """

    n_chunks = (df.shape[0] + chunk_size - 1) // chunk_size

    chunks = range(n_chunks)
    if verbose:
        if 'tqdm' in globals():
            chunks = tqdm(chunks)
        else:
            logging.warning('`tqdm` is not installed')

    for i in chunks:
        with db.begin() as tx:
            sql_dump_from_pandas(tx, df[i * chunk_size:(i + 1) * chunk_size], table)
            tx.commit()



_refine_column_type_hints = dict(
    number_like = {
        sqlt.Numeric,
        sqlt.SmallInteger, sqlt.Integer, sqlt.BigInteger,
        sqlt.Float},
    string_like = {
        sqlt.String,
        sqlt.Unicode,
        sqlt.Text},
    date_like = {
        sqlt.Date,
        sqlt.DateTime},
    none = {none_type})

@multimethod(
    key = lambda t: _c(
        fst, fst,
        L + [(str(t),)], list,  # type: ignore
        _filter(_r(op.contains, t), snd))(
            _refine_column_type_hints.items()))
def refine_column_type(cfg, t, xs):
    pass

is_bounded = lambda b, xs: _c(np.all, L < b, np.abs, _r(np.nan_to_num, 0), L.astype(np.float64))(xs)

is_null = lambda x: (x is None) or (x is pd.NA) or (x is pd.NaT) or (x is np.nan)


@refine_column_type.register
def refine_column_type_number_like(
        cfg,
        t: Optional[sqlt.TypeEngine],
        xs: Union[np.ndarray, pd.Series]):

    xs = _c(np.array, list, _filter(op.not_, is_null), list)(xs)

    if _c(np.all, np.isnan, L.astype(np.float64))(xs):
        return sqlt.Numeric()

    if not is_bounded(cfg['bigint_threshold'], xs):
        return sqlt.Numeric()

    if _c(np.any, L > cfg['float_threshold'],
          np.abs, _r(np.nan_to_num, 0),
          spread(op.sub), fanout(np.round, identity), L.astype(float))(xs):
        return sqlt.Float()

    if is_bounded(cfg['smallint_threshold'], xs):
        return sqlt.SmallInteger()

    if is_bounded(cfg['integer_threshold'], xs):
        return sqlt.Integer()

    return sqlt.BigInteger()


@refine_column_type.register
def refine_column_type_string_like(
        cfg,
        t: Optional[sqlt.TypeEngine],
        xs: Union[np.ndarray, pd.Series]):

    if _c(np.all, L == None, np.array)(xs):
        return sqlt.String()

    return sqlt.String(length = max(
        _c(int, np.nanmax, np.vectorize(lambda x: float(len(x)) if x is not None else np.nan))(xs),
        (P_.length(t) or 0)))


# TODO: timezones?
@refine_column_type.register
def refine_column_type_date_like(
        cfg,
        t: Optional[sqlt.TypeEngine],
        xs: Union[np.ndarray, pd.Series]):

    if _c(np.all, L == None, np.array)(xs):
        return sqlt.Date()

    xs1 = _c(
        L.astype('datetime64[s]'), np.array,
        list, _map(lambda x: None if x is pd.NaT else x))(xs)

    dts = xs1 - xs1.astype('datetime64[D]')

    if (dts.astype(np.int64) > 0).any():
        return sqlt.DateTime()

    return sqlt.Date()


@refine_column_type.register
def refine_column_type_none(
        cfg,
        t: Optional[sqlt.TypeEngine],
        xs: Union[np.ndarray, pd.Series]):

    return None


_type_digest_hints = {
    type(None): None,
    int: float,
    np.int64: float,
    np.float64: float,
    np.str_: str,
    type(pd.NaT): datetime.datetime,
    type(pd.NA): float,
    np.datetime64: datetime.datetime,
    pd.Timestamp: datetime.datetime}


mk_type_digest = _c(
    tuple, _sort(str),
    list, _filter(is_just),
    set, _map(lambda x: _type_digest_hints.get(x, x)),
    set, _map(type))
"""Returns hashable set of types in the given array.

"""

type_digest_mapping = {
    (): none_type,
    (datetime.datetime,): sqlt.Date,
    (float,): sqlt.Numeric,
    (int,): sqlt.Numeric,
    (float, int): sqlt.Numeric,
    (str,): sqlt.String}


def detect_column_type_hint(xs) -> Optional[sqlt.TypeEngine]:
    """Attempts to detect a generic sql type for ``xs`` without any hints.

    """

    return type_digest_mapping.get(mk_type_digest(xs))


def detect_column_type(xs, column: Optional[Column] = None) -> Optional[sqlt.TypeEngine]:
    """Attempts to detect a generic sql type for ``xs`` using ``column`` metadata if presented.

    """

    cfg = dict(
        float_threshold = 1e-8,
        smallint_threshold = 2**(16 - 1)/10,
        integer_threshold = 2**(32 - 1)/10,
        bigint_threshold = 2**(64 - 1)/10)

    t = (detect_column_type_hint(xs)
         if column is None
         else type(column.type.as_generic()))  # type: ignore

    return (None if t == none_type
            else (refine_column_type(cfg, t, xs) or t))


def detect_column_types(df: pd.DataFrame, table: Optional[Table] = None):
    """Attempts to detect generic sql types for ``table`` and it's sample ``df``.

    ``df`` should not contain empty columns. To generate a sample that adheres
    this requirement use ``sql_fetch_sample`` function.
    """

    if table is None:
        return [Column(c, detect_column_type(df[c])) for c in df.columns]

    return [
        Column(c, detect_column_type(df[c], table.columns[c]))
        for c in list(set(table.columns.keys()).intersection(set(df.columns)))]


def sql_fetch_sample(tx: Connection, table: Table, sample_size: int = 10000, filters = []):
    """Fetches representative sample from ``table`` for each column.

    This function attempts to fetch rows that contain non-null values for each
    column separately, then unites them. Thus the exact size might differ from
    ``sample_size``.
    """

    n_cols = len(table.columns.keys())
    n_rows = (sample_size + n_cols - 1) // n_cols

    df = sql_load_to_pandas(
        tx, sql.union(*[
            table.select().where(c.is_not(None), *filters).limit(n_rows)  # type: ignore
            for c in table.columns.values()]), coerce_dates = False)

    null_columns = _c(list, _map(fst), _filter(snd), L.iteritems())((~df.isna()).sum() == 0)  # type: ignore

    if len(null_columns) > 0:
        logging.warning('unable to fetch non-null values for the columns: ' + ', '.join(null_columns))

    return df

@multimethod(key = lambda p: p if isinstance(p, str) else p[0])
def partition_values_query(column, p):
    pass

@partition_values_query.register
def partition_values_query_distinct(column, p):
    return column

@partition_values_query.register
def partition_values_query_date_trunc(column, p):
    return date_trunc(p[1], column)


@partition_values_query.register
def partition_values_query_mod(column, p):
    return sql.func.mod(column, p[1])


@partition_values_query.register
def partition_values_query_prefix(column, p):
    return sql.func.substr(column, 1, p[1])


@partition_values_query.register
def partition_values_query_suffix(column, p):
    return sql.func.substr(column, sql.func.length(column) - p[1], p[1])


def sql_get_partition_values(tx, column, p):
    return _c(list, _sort(), _map(L[0]), L.fetchall(), sql_exec)(
        tx, sql.select(partition_values_query(column, p)).distinct())


def sql_copy_table_metadata(db_src, db_dst, table_src, md_dst, **kwargs):
    """Constructs a new table in ``md_dst`` similar to ``table_src`` and optimized for ``db_dst``.

    """

    df = None
    with db_src.begin() as tx:
        df = sql_fetch_sample(tx, table_src, **kwargs)

    cs = detect_column_types(df, table_src)

    if db_dst.dialect.name == 'postgresql':
        cs_ordered = sql_pg_align_columns(cs)
        logging.info('reordering columns, row padding: {} -> {}'.format(*map(sql_pg_padding_loss, [cs, cs_ordered])))
        cs = cs_ordered

    return Table(table_src.name, md_dst, *cs)

# TODO: add support for NULL values
def sql_get_grid_values(tx: Connection, table: Table, grid_spec: OrderedDict):
    """Generates partitioning range based on ``table`` contents and grid specification.

    ``grid_spec`` is an ``OrderedDict`` of which the keys are column names to
    split on and values are splitter definitions. A splitter definition can be one of the following:

    - ``'distinct'``
    - ``('prefix', n)`` -- first ``n`` symbols of the column value
    - ``('suffix', n)`` -- last ``n`` symbols of the column value
    - ``('date_trunc', interval)`` where ``interval`` is ``'month'``, ``'week'``, ``'day'``, etc.
    - ``('mod', n)`` where n is a positive integer number
    """

    result = OrderedDict()
    for c, p in grid_spec.items():
        vs = sql_get_partition_values(tx, table.c[c], p)
        if len(vs) <= 1:
            logging.warning(f'partition for `{c}` by `{p}` has no effect')
        else:
            result[c] = vs
    return result


def sql_copy_table(db_src, db_dst, table_src, table_dst):
    """Copies data from ``table_src`` to ``table_dst`` within a single transaction.

    This method suits well for small tables that do not require any preparations.
    """

    df = None
    with db_src.begin() as tx:
        df = sql_load_to_pandas(tx, sql.select(table_src), coerce_dates = False)

    logging.info(f'loaded {df.shape[0]} rows')

    with db_dst.begin() as tx:
        sql_dump_from_pandas(tx, df, table_dst)
        tx.commit()


def sql_copy_partition(db_src, db_dst, table_src, table_dst, grid_spec, partition, verbose = True):
    """Copies data from ``table_src`` to ``table_dst`` only for ``partition``.

    ``partition`` is a dictionary of parameters from the range generated by
    ``sql_get_grid_values``.
    Data transferring happens within a single transaction. If ``table_dst``
    contains any data within the ``partition`` then the execution is aborted.
    """

    if verbose:
        logging.info(f'copying partition {grid_spec}')

    with db_dst.begin() as tx:
        exists = _c(L > 0, len, L.fetchall(), sql_exec)(
            tx,
            (sql.select(sql.literal_column('1'))
             .where(table_dst.select()
                    .where(*[
                        (partition_values_query(table_dst.c[c], grid_spec[c]) == v)
                        for c, v in partition.items()]).exists())))
        if exists:
            if verbose:
                logging.info('already exists, skipping')
            return

    if verbose:
        logging.info('loading data')

    df = None
    with db_src.begin() as tx:
        df = sql_load_to_pandas(
            tx, sql.select(table_src).where(*[
                (partition_values_query(table_src.c[c], grid_spec[c]) == v)
                for c, v in partition.items()]),
            coerce_dates = False)

    if verbose:
        logging.info(f'loaded {df.shape[0]} rows')

    with db_dst.begin() as tx:
        sql_dump_from_pandas(tx, df, table_dst)
        tx.commit()


def sql_copy_partitions(
        db_src, db_dst,
        table_src, table_dst,
        grid_spec, partitions,
        verbose = True,
        n_workers = 1, worker_num = 0,
        n_retries = 3,
        on_update = None):
    """Copies data from ``table_src`` to ``table_dst`` for all ``partitions``.

    Also facilitates parallel execution. To run on disjoint sets specify
    ``n_workers > 0`` and ``worker_num`` in ``range(n_workers)``. To monitor
    progress use callback ``on_update``.
    """

    for i, p in enumerate(partitions):
        if i % n_workers == worker_num:
            if verbose:
                logging.info(f'processing partition #{i}: `{p}`')

            for j in range(1 + n_retries):
                try:
                    sql_copy_partition(db_src, db_dst, table_src, table_dst, grid_spec, p, verbose)
                    break
                except Exception as e:
                    logging.error(f'worker #{worker_num}: {e}')
                    logging.info(f'worker #{worker_num}: ' + ('retrying' if j < n_retries else 'skipping'))

            if on_update is not None:
                on_update()


__all__ = [
    'sql_load_to_pandas',
    'sql_coerce_type',
    'sql_dump_from_pandas',
    'sql_dump_from_pandas_chunked',
    'refine_column_type',
    'mk_type_digest',
    'detect_column_type_hint',
    'detect_column_type',
    'detect_column_types',
    'sql_fetch_sample',
    'partition_values_query',
    'sql_get_partition_values',
    'sql_copy_table_metadata',
    'sql_copy_table',
    'sql_copy_partition',
    'sql_copy_partitions']
