from ad_hoc_utils.common.imp import extras

extras.require_deps(__name__, 'oracle')


from typing import Callable, Union, Optional, List

import logging

from pathlib import Path
import base64
from functools import wraps, partial
from getpass import getpass
import datetime
from bs4 import BeautifulSoup
import requests
import textwrap

import sqlalchemy as sql
import sqlalchemy.types as sqlt
from sqlalchemy import create_engine, Table, Column
from sqlalchemy.engine import Engine, Connection
from sqlalchemy.schema import CreateTable, DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.dml import Executable
from sqlalchemy.sql.expression import BindParameter, FunctionElement


import operator as op
from collections import OrderedDict

from ad_hoc_utils.common.f import L, P, P_, _r, _c, _filter, _map, _sort, fanout, ignore_unused, is_just, fst, snd
from ad_hoc_utils.common.f import lift_maybe, named, spread, void, first, identity, none_type
from itertools import chain, repeat


from .common import connect_to_db


@compiles(CreateTable, 'oracle')
def _create_table_oracle_tablespace(element, compiler, **kw):
    text = compiler.visit_create_table(element, **kw)

    tablespace = (
        element.element.info.get('tablespace')
        or element.element.metadata.info.get('tablespace'))

    if tablespace is not None:
        text += textwrap.dedent(f'tablespace {tablespace}').strip()

    return text


@compiles(DropTable, 'oracle')
def _drop_table_oracle_purge(element, compiler, **kw):
    text = compiler.visit_drop_table(element, **kw)

    purge = element.element.info.get('purge')
    if purge is None:
        purge = element.element.metadata.info.get('purge') or False

    if purge:
        text += ' PURGE'

    return text


class date_trunc(FunctionElement):
    name = 'date_trunc'
    inherit_cache = True


date_trunc_mapping = dict(
    minute = 'mi',
    hour = 'hh',
    day = 'dd',
    week = 'iw',
    month = 'mm',
    quarter = 'q',
    year = 'yyyy',
    century = 'cc')


@compiles(date_trunc)
def compile_date_trunc_default(element, compiler, **kw):
    return 'date_trunc({})'.format(compiler.process(element.clauses, **kw))


@compiles(date_trunc, 'oracle')
def compile_date_trunc_oracle(element, compiler, **kw):
    interval, arg = list(element.clauses)
    return 'trunc({}, {})'.format(
        compiler.process(arg, **kw),
        f"'{date_trunc_mapping[interval.value]}'"
        if isinstance(interval, BindParameter)
        else compiler.process(sql.case([(arg == k, v) for k, v in date_trunc_mapping.items()])))


@connect_to_db.register
def connect_to_db_oracle(
        *,
        asyncio: bool,
        host: str,
        sid: str,
        user: str,
        password: str,
        **kwargs) -> Engine:
    """Creates Oracle DB engine.

    Connection-specific arguments: ``host``, ``sid``, ``user``.
    """

    assert (not asyncio), 'There is no support for async Oracle engine'
    return (
        dict(host = host, sid = sid, user = user, password = password),
        kwargs)


__all__ = []
