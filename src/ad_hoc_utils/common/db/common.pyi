
from sqlalchemy.engine import Engine
from sqlalchemy.ext.asyncio import AsyncEngine
from ad_hoc_utils.common.config import BaseFetcher


def connect_to_db(
        db_type, *,
        asyncio: bool = False,
        password: str | BaseFetcher | None = None,
        **kwargs) -> Engine | AsyncEngine: ...

