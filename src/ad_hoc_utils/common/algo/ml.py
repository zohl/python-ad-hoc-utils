from ad_hoc_utils.common.imp import extras

extras.require_deps(__name__, 'ml')

from typing import Callable, TypeVar

import numpy as np
import logging

from ad_hoc_utils.common.f import spread, iterate, min_by, snd
from more_itertools import take

T = TypeVar('T')

_sa_default_probability = lambda e0, e1, t: 1 if e1 < e0 else np.exp(-(e1 - e0)/t)
_sa_default_temperature = lambda i, n_iter: 1 - i/n_iter


def simulated_annealing(
        s0: T,
        evaluate: Callable[[T], float],
        mutate: Callable[[T, np.random.Generator, float], T],
        *,
        n_iter: int,
        seed: int = None,
        verbose = False,
        probability: Callable[[float, float, float], float] = _sa_default_probability,
        temperature: Callable[[int, int], float] = _sa_default_temperature):

    rng = np.random.default_rng(seed)

    @spread
    def step(s0, e0, i):
        t = temperature(i, n_iter)
        s = mutate(s0, rng, t)
        e = evaluate(s)

        if verbose:
            logging.info(f'val: {e}')

        p = probability(e0, e, t)
        r = rng.uniform()

        return (s, e, i + 1) if (r <= p) else (s0, e0, i + 1)

    (s_best, e_best, i) = min_by(snd, take(n_iter, iterate(step, (s0, evaluate(s0), 0))))

    if verbose:
        logging.info(f'done, best result: {e_best}')
    return s_best


__all__ = [
    'simulated_annealing']
