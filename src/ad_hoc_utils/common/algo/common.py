from typing import Callable, TypeVar

from collections import defaultdict


from ad_hoc_utils.common.f import _l, fst, second, first, _c, spread, constant, cond, _map, snd, min_by, iterate
from more_itertools import partition, take

T = TypeVar('T')


def binary_search(f: Callable[[float], bool], l: float, r: float, eps: float) -> float:
    while True:
        x = (l + r) / 2
        if abs(r - l) < eps:
            return x

        y = f(x)
        if y:
            r = x
        else:
            l = x


def ternary_search(f: Callable[[float], int], l: float, r: float, eps: float) -> float:
    # TODO
    ...



class keydefaultdict(defaultdict):
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        else:
            val = None
            if type(key) != tuple:
                val = self.default_factory(key)
            else:
                val = self.default_factory(*key)

            self[key] = val
            return val


def mk_dp_solver(*handlers):
    """
    Solves a dynamic programming task, specified by ``handlers``.

    """

    static_handlers, dynamic_handlers = _c(
        list, _map(list),
        _c(second, _map)(
            first(spread),
            second(lambda x: x if callable(x) else constant(x))),
        _l(partition, _c(callable, fst)),
        _map(lambda x: x if type(x) == tuple else (constant(True), x)))(
        handlers)

    d = keydefaultdict(lambda *key: cond(key, *dynamic_handlers)(d, *key))
    for (key, v) in static_handlers:
        d[key] = v
    return d


__all__ = [
    'binary_search', 'ternary_search',
    'keydefaultdict',
    'mk_dp_solver']
