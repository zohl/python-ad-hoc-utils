import logging
import operator as op
from ad_hoc_utils.common.f import function, ellipsis, condp, _l


class codec:
    def __init__(self, encode, decode):
        self._encode = encode
        self._decode = decode


def run_codec(cfg, x, handler = None):
    rec = _l(run_codec, cfg)

    handler = (
        condp(handler, op.eq, *cfg['handlers'])
        or handler
        or condp(type(x), op.is_, *cfg['handlers']))

    if handler is None:
        logging.info(f'no handler for value `{x} :: {type(x)}`')
        return

    meta_handler = condp(type(handler), op.is_, *cfg['meta_handlers'])
    if meta_handler is None:
        logging.info(f'no meta-handler for handler of type `{type(handler)}`')
        return

    return meta_handler(rec, x, handler)


_as_is = lambda f, x: x
_list_map = lambda f, x: [f(v) for v in x]
_dict_map = lambda f, x: {f(k): f(v) for k, v in x.items()}


json_default_meta_handlers = [
    (function, lambda f, x, handler: handler(f, x)),
    (dict, lambda f, x, handler: {k: f(v, handler.get(k)) for k, v in x.items()}),
    (list, lambda f, x, handler: [f(v, handler[0]) for v in x]),
    (tuple, lambda f, x, handler: tuple([f(v, h) for v, h in zip(x, handler)])),
    (ellipsis, lambda f, x, handler: f(x, None))]


json_default_handlers = [
    (int, _as_is),
    (str, _as_is),
    (bool, _as_is),
    (float, _as_is),
    (list, _list_map),
    (tuple, _list_map),
    (dict, _dict_map)]


def to_json(x, *, handler = None, extra_handlers = []):
    return run_codec(
        dict(
            handlers = [*json_default_handlers, *extra_handlers],
            meta_handlers = [
                *json_default_meta_handlers,
                (codec, lambda f, x, handler: handler._encode(f, x))]),
        x, handler)


def from_json(x, *, handler = None, extra_handlers = []):
    return run_codec(
        dict(
            handlers = [*json_default_handlers, *extra_handlers],
            meta_handlers = [
                *json_default_meta_handlers,
                (codec, lambda f, x, handler: handler._decode(f, x))]),
        x, handler)


__all__ = [
    'codec',
    'run_codec',
    'to_json', 'from_json']
