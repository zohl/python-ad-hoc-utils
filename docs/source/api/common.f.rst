Functional tools
================


Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.


High-order functions
--------------------

.. automodule:: ad_hoc_utils.common.f
   :members: lcurry, rcurry, uncurry, _l, _r, identity, constant, swap, spread,
             unspread, apply, compose, _c, composable, _map, _filter, _sort
   :undoc-members:
   :noindex:


Decorators
----------

.. automodule:: ad_hoc_utils.common.f
   :members: with_optional_params
   :undoc-members:
   :noindex:


Multimethods
------------

.. automodule:: ad_hoc_utils.common.f
   :members: TODO
   :undoc-members:
   :noindex:
