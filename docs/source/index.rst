.. include:: ../../README.rst


.. toctree::
   :maxdepth: 2
   :caption: Core tutorials:
   :hidden:

   tutorials/core/database
   tutorials/core/imports
   tutorials/core/functional_tools


.. toctree::
   :maxdepth: 2
   :caption: ML tutorials:
   :hidden:

   tutorials/ml/data_preparation
   tutorials/ml/feature_extraction
   tutorials/ml/model_selection


.. toctree::
   :maxdepth: 2
   :caption: API reference:
   :hidden:

   api/common.f
   api/common.db
   api/common.imp
   api/ml.binning
   api/ml.metrics


.. toctree::
   :maxdepth: 2
   :caption: Contributing:
   :hidden:

   contrib/workflow
   contrib/guidelines
