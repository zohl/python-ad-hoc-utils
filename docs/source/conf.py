import os
import sys
sys.path.insert(0, os.path.abspath('../../src'))


project = 'ad-hoc utils'
copyright = '2022, ___'
author = '___'


extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.autosummary',
    'sphinx.ext.napoleon'
]

autodoc_typehints = 'description'
autodoc_typehints_format = 'short'
autodoc_member_order = 'bysource'
autodoc_typehints_description_target = 'documented'
autodoc_type_aliases = dict()

napoleon_google_docstring = False
napoleon_use_param = False
napoleon_use_ivar = True

templates_path = []
html_static_path = []
exclude_patterns = []

html_theme = 'sphinx_rtd_theme'

