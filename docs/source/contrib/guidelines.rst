Guidelines
==========


Before you push the changes
---------------------------

Make sure you don't introduce:

- trailing spaces

- tabs as indentation

- test regressions (see :ref:`ref-workflow-testing`, full check)

- docs building errors (see :ref:`ref-workflow-building-the-docs`)


Otherwise the library quality will quickly degrade.


Recommended practices
---------------------

Since the library is experimental and the code is volatile, you might not want
to overexercise the good practices. Still, some parts eventually become stable
and you might want to accelerate this process.

Here is a roadmap (ordered by amount of work to do, from least to most):

- add smoke tests
- add unit tests
- make the code follow pep8 recommendations
- add docstrings to functions and classes
- add user acceptance tests
- add api reference entry in documentation
- add type annotations


PEP8
----

To turn off some ``flake8`` warnings permanently, specify them in ``setup.cfg`` file:

.. code-block:: ini

   [flake8]
   ignore = E741, E731, E251, W503


To do so for a single line, specify ``#noqa:`` comment at the end of the line:

.. code-block:: python

   dtc_hyperparams_space = dict(
       max_depth         = (int,   (1,     16)),   # noqa: E221,E241
       min_samples_leaf  = (float, (1e-12, 0.5)),  # noqa: E221,E241
       min_samples_split = (float, (1e-12, 1.0)))  # noqa: E221,E241


Typing
------


To disable typechecks, specify ``# type: ignore`` at the end of the line:

.. code-block:: python

   fst = L[0]  # type: ignore

   snd = L[1]  # type: ignore



Tests
-----

- TODO: smoke tests
- TODO: unit tests
- TODO: uat
