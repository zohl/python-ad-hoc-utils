Workflow
========


Setting up the working environment
----------------------------------

Clone the repo and install it in development mode:

.. code-block:: bash

   git clone ....
   pip install -e ./python-ad-hoc-utils


Create Jupyter notebook ``dev.ipynb`` inside the repo, this filename is already in ``.gitignore``.


You can access library's data as usual:

>>> from ad_hoc_utils.ml.binning import mk_bins

And you can access test's data by importing ``tests`` module:

>>> from tests.ml.binning.tests import *



.. _ref-workflow-testing:

Testing
-------

**Synopsis**:

.. code-block:: bash

   # quick check (no syntax errors and missing imports)
   tox -e smoke-py310

   # run all unit-tests
   tox -e unit-py310

   # run unit-tests from specific file until the first error
   tox -e unit-py310 -- -x ./tests/common/test_f.py

   # run functional tests
   tox -e uat-py310

   # full check
   for e in {smoke,unit,uat}-{py39,py310}; do tox -e "$e"; done

You can test ``ad_hoc_utils`` by executing pre-defined tox environments.
There are 3 types of test sets (``smoke``, ``unit`` and ``uat``) with version-specific factors (``py39`` and ``py310``).


.. _ref-workflow-static-checks:


Static checks
-------------

**Synopsis**:

.. code-block:: bash

   tox -e static


Tox runs ``mypy`` and ``flake8`` against the library and tests in ``python-3.10`` environement.


.. _ref-workflow-building-the-docs:

Building the docs
-----------------

**Synopsis**:

.. code-block:: bash

   tox -e docs

Tox builds the documentation in ``python-3.10`` environement.
