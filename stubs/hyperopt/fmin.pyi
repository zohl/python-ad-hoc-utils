from typing import Any

def fmin(fn, space, algo: Any | None = ..., max_evals: Any | None = ..., timeout: Any | None = ..., loss_threshold: Any | None = ..., trials: Any | None = ..., rstate: Any | None = ..., allow_trials_fmin: bool = ..., pass_expr_memo_ctrl: Any | None = ..., catch_eval_exceptions: bool = ..., verbose: bool = ..., return_argmin: bool = ..., points_to_evaluate: Any | None = ..., max_queue_len: int = ..., show_progressbar: bool = ..., early_stop_fn: Any | None = ..., trials_save_file: str = ...): ...

